package org.c4.userservice.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.c4.microservice.framework.domainevent.DomainEventSink;
import org.c4.microservice.framework.domainevent.communication.DomainEvent;
import org.c4.microservice.framework.domainevent.messaging.EventPublisher;
import org.c4.userservice.ErrorKey;
import org.c4.userservice.identityprovider.KeycloakAttributeBuilder;
import org.c4.userservice.user.data.*;
import org.c4.userservice.user.events.UserCreatedEvent;
import org.c4.userservice.user.events.UserStatusUpdatedEvent;
import org.c4.userservice.user.persistence.SpringDataUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.*;

import static org.c4.userservice.user.mapping.UserMappingValue.PRIVATE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration-Test für die User-API.
 * <br/>
 * Copyright: Copyright (c) 16.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest
{
    private final String AGGREGATE_ID            = UUID.randomUUID().toString();
    private final String USER_ID                 = UUID.randomUUID().toString();
    private final String USERNAME                = "username";
    private final String EMAIL                   = "user@email.de";
    private final String USER_TYPE_ATTRIBUTE_KEY = "userType";

    private final String CREATE_URL        = "/users";
    private final String USER_URL          = "/users/{id}";
    private final String UPDATE_STATUS_URL = "/users/{id}/status";
    private final String TYPE_URL          = USER_URL + "/type";

    @Autowired
    private DomainEventSink          domainEventSink;
    @Autowired
    private SpringDataUserRepository springDataUserRepository;

    @Autowired
    private MockMvc      mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private EventPublisher eventPublisher;
    @MockBean
    private RealmResource  realmResource;
    @Mock
    private UsersResource  usersResource;

    @BeforeEach
    void setUp()
    {
        doAnswer(invocation -> {
            DomainEvent domainEvent = invocation.getArgument(0);
            domainEvent.setVersion(
                    domainEvent.getVersion() == null ? 0L : domainEvent.getVersion() + 1);
            domainEventSink.consumeDomainEvent(invocation.getArgument(0));
            return null;
        }).when(eventPublisher).send(any());

        doReturn(usersResource).when(realmResource).users();

        springDataUserRepository.deleteAll();
    }

    /**
     * Ein User wird korrekt erstellt. Ein Event für ein UserMapping wird erstellt und verarbeitet.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void createUser_success() throws Exception
    {
        doReturn(Response.created(URI.create(".../users/" + USER_ID)).build()).when(usersResource)
                .create(any());
        doReturn(Collections.singletonList(createUserRepresentation())).when(usersResource)
                .search(anyString());

        UserCreateRestDto restDto = createRestDto();

        mockMvc.perform(post(CREATE_URL).contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(restDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.result.id", is(equalTo(USER_ID))));

        Optional<UserEntity> insertedMapping = springDataUserRepository.findByUserPersistenceDto_userId(
                USER_ID);

        assertTrue(insertedMapping.isPresent());
        verify(eventPublisher, times(1)).send(any(UserCreatedEvent.class));
    }

    /**
     * Wenn Email oder Username bereits belegt sind, liefert Keycloak einen Fehler, der entsprechend
     * propagiert wird.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void createUser_nameOrEmailTaken() throws Exception
    {
        doReturn(Response.status(Response.Status.CONFLICT).build()).when(usersResource)
                .create(any());

        UserCreateRestDto restDto = createRestDto();

        mockMvc.perform(post(CREATE_URL).contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(restDto)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", contains(ErrorKey.USERNAME_OR_EMAIL_TAKEN)));

        Optional<UserPersistenceDto> insertedMapping = springDataUserRepository.findById(USER_ID)
                .map(UserEntity::getUserPersistenceDto);

        assertFalse(insertedMapping.isPresent());
        verify(eventPublisher, never()).send(any(UserCreatedEvent.class));
    }

    /**
     * Wenn durch Keycloak ein anderer Status zurückgegeben wird, wird dies korrekt abgefangen.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void createUser_otherKeycloakResponse() throws Exception
    {
        doReturn(Response.status(Response.Status.BAD_REQUEST).build()).when(usersResource)
                .create(any());

        UserCreateRestDto restDto = createRestDto();

        mockMvc.perform(post(CREATE_URL).contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(restDto)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", contains(ErrorKey.UNDEFINED_KEYCLOAK_ERROR)));

        Optional<UserPersistenceDto> insertedMapping = springDataUserRepository.findById(USER_ID)
                .map(UserEntity::getUserPersistenceDto);

        assertFalse(insertedMapping.isPresent());
        verify(eventPublisher, never()).send(any(UserCreatedEvent.class));
    }

    /**
     * Wenn durch Keycloak eine Exception auftritt, wird dies korrekt abgefangen.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void createUser_otherKeycloakError() throws Exception
    {
        doAnswer(invocation -> {
            throw new Exception();
        }).when(usersResource).create(any());

        UserCreateRestDto restDto = createRestDto();

        mockMvc.perform(post(CREATE_URL).contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(restDto)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", contains(ErrorKey.UNDEFINED_KEYCLOAK_ERROR)));

        Optional<UserPersistenceDto> insertedMapping = springDataUserRepository.findById(USER_ID)
                .map(UserEntity::getUserPersistenceDto);

        assertFalse(insertedMapping.isPresent());
        verify(eventPublisher, never()).send(any(UserCreatedEvent.class));
    }

    /**
     * User wird korrekt aktualisiert.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void updateUserManaged_success() throws Exception
    {
        doReturn(createUserResource()).when(usersResource).get(USER_ID);

        UserManagedUpdateRestDto restDto = createUserManagedUpdateRestDto();

        mockMvc.perform(put(USER_URL, USER_ID).contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(restDto)))
                .andExpect(status().isNoContent());
    }

    /**
     * Wird der User nicht gefunden, wird dies korrekt weitergegeben.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void updateUserManaged_notFound() throws Exception
    {
        doReturn(null).when(usersResource).get(USER_ID);

        UserManagedUpdateRestDto restDto = createUserManagedUpdateRestDto();

        mockMvc.perform(put(USER_URL, USER_ID).contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(restDto)))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.errors[*]", contains(ErrorKey.USER_NOT_FOUND)));
    }

    /**
     * Tritt ein Fehler im Keycloak auf, wird dies korrekt weitergegeben.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void updateUserManaged_keycloakError() throws Exception
    {
        UserResource userResource = createUserResource();
        doAnswer(invocation -> {
            throw new Exception();
        }).when(userResource).update(any());
        doReturn(userResource).when(usersResource).get(USER_ID);

        UserManagedUpdateRestDto restDto = createUserManagedUpdateRestDto();

        mockMvc.perform(put(USER_URL, USER_ID).contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(restDto)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[*]", contains(ErrorKey.UNDEFINED_KEYCLOAK_ERROR)));
    }

    /**
     * Führt das Update nicht zu einer Veränderung, wird es nicht angewendet, stattdessen wird ein
     * Fehler geworfen.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void updateUserStatus_identical() throws Exception
    {
        UserEntity userEntity = createUserEntity();
        springDataUserRepository.save(userEntity);

        UserStatusRestDto restDto = new UserStatusRestDto();
        restDto.setUserStatus(UserStatus.ACTIVE.name());

        mockMvc.perform(put(UPDATE_STATUS_URL, USER_ID).contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(restDto)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[*]", contains(ErrorKey.UPDATE_IDENTICAL)));

        verify(eventPublisher, never()).send(any());
    }

    /**
     * Wird der User nicht gefunden, wird dies korrekt zurückgegeben.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void updateUserStatus_userNotFound() throws Exception
    {
        UserStatusRestDto restDto = new UserStatusRestDto();
        restDto.setUserStatus(UserStatus.ACTIVE.name());

        mockMvc.perform(put(UPDATE_STATUS_URL, USER_ID).contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(restDto)))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.errors[*]", contains(ErrorKey.USER_NOT_FOUND)));

        verify(eventPublisher, never()).send(any());
    }

    /**
     * User wird korrekt über ein Event aktualisiert.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void updateUserStatus_success() throws Exception
    {
        UserEntity userEntity = createUserEntity();
        springDataUserRepository.save(userEntity);

        UserStatusRestDto restDto = new UserStatusRestDto();
        restDto.setUserStatus(UserStatus.INACTIVE.name());

        mockMvc.perform(put(UPDATE_STATUS_URL, USER_ID).contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(restDto)))
                .andExpect(status().isNoContent())
                .andExpect(jsonPath("$.errors[*]").doesNotExist());

        ArgumentCaptor<UserStatusUpdatedEvent> argumentCaptor = ArgumentCaptor.forClass(
                UserStatusUpdatedEvent.class);
        verify(eventPublisher, times(1)).send(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getChangeDto().getUserStatus(),
                is(equalTo(UserStatus.INACTIVE)));

        Optional<UserEntity> result = springDataUserRepository.findByUserPersistenceDto_userId(
                USER_ID);
        assertThat(result.isPresent(), is(equalTo(true)));
        assertThat(result.get().getUserPersistenceDto().getUserStatus(),
                is(equalTo(UserStatus.INACTIVE)));
    }

    /**
     * User wird anhand seiner ID gefunden.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void getUserById_success() throws Exception
    {
        UserResource userResource = mock(UserResource.class);
        doReturn(userResource).when(usersResource).get(anyString());

        UserRepresentation userRepresentation = createUserRepresentation();
        doReturn(userRepresentation).when(userResource).toRepresentation();

        mockMvc.perform(get(USER_URL, USER_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result.userId", is(equalTo(USER_ID))))
                .andExpect(jsonPath("$.result.username", is(equalTo(USERNAME))))
                .andExpect(jsonPath("$.result.email", is(equalTo(EMAIL))));
    }

    /**
     * User kann nicht gefunden werden.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void getUserById_notFound() throws Exception
    {
        UserResource userResource = mock(UserResource.class);
        doReturn(userResource).when(usersResource).get(anyString());

        doReturn(null).when(userResource).toRepresentation();

        mockMvc.perform(get(USER_URL, USER_ID)).andExpect(status().isNotFound());
    }

    /**
     * Keycloak produziert einen Error. Dieser wird korrekt abgefangen.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void getUserById_errorThrown() throws Exception
    {
        doAnswer(invocation -> {
            throw new Exception();
        }).when(usersResource).get(anyString());

        mockMvc.perform(get(USER_URL, USER_ID)).andExpect(status().isNotFound());
    }

    /**
     * Type des Users wird korrekt abgefragt.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "backend")
    void getTypeForUser_success() throws Exception
    {
        UserResource userResource = mock(UserResource.class);
        doReturn(userResource).when(usersResource).get(anyString());

        UserRepresentation userRepresentation = createUserRepresentation();
        doReturn(userRepresentation).when(userResource).toRepresentation();

        mockMvc.perform(get(TYPE_URL, USER_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(equalTo(UserType.MEMBER.name()))));
    }

    /**
     * Type kann nicht ermittelt werden, weil der User nicht gefunden wurde.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "backend")
    void getTypeForUser_userNotFound() throws Exception
    {
        doReturn(null).when(usersResource).get(anyString());

        mockMvc.perform(get(TYPE_URL, USER_ID))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.errors[*]", contains(ErrorKey.USER_NOT_FOUND)));
    }

    private UserManagedUpdateRestDto createUserManagedUpdateRestDto()
    {
        UserManagedUpdateRestDto restDto = new UserManagedUpdateRestDto();
        restDto.setUserType(UserType.MEMBER.name());

        return restDto;
    }

    private UserCreateRestDto createRestDto()
    {
        UserCreateRestDto userRestDto = new UserCreateRestDto();
        userRestDto.setUsername(USERNAME);
        userRestDto.setEmail(EMAIL);
        userRestDto.setUserType(UserType.MEMBER.name());

        return userRestDto;
    }

    private UserEntity createUserEntity()
    {
        UserEntity userEntity = new UserEntity();
        userEntity.setVersion(1L);
        userEntity.setUserPersistenceDto(createUserPersistenceDto());

        return userEntity;
    }

    private UserPersistenceDto createUserPersistenceDto()
    {
        UserPersistenceDto userPersistenceDto = new UserPersistenceDto();
        userPersistenceDto.setUserId(USER_ID);
        userPersistenceDto.setEmail(PRIVATE);
        userPersistenceDto.setUserType(PRIVATE);
        userPersistenceDto.setUserStatus(UserStatus.ACTIVE);
        userPersistenceDto.setUsername(PRIVATE);

        return userPersistenceDto;
    }

    private UserRepresentation createUserRepresentation()
    {
        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setId(USER_ID);
        userRepresentation.setUsername(USERNAME);
        userRepresentation.setEmail(EMAIL);
        Map<String, List<String>> attributeMap = KeycloakAttributeBuilder.empty()
                .addKey(USER_TYPE_ATTRIBUTE_KEY, UserType.MEMBER.name())
                .build();
        userRepresentation.setAttributes(attributeMap);

        return userRepresentation;
    }

    private UserResource createUserResource()
    {
        UserResource userResource = mock(UserResource.class);
        doReturn(createUserRepresentation()).when(userResource).toRepresentation();

        return userResource;
    }
}
