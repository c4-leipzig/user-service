package org.c4.userservice.user;

import org.apache.http.auth.BasicUserPrincipal;
import org.c4.microservice.framework.domainevent.messaging.EventPublisher;
import org.c4.userservice.user.data.*;
import org.c4.userservice.user.events.UserCreatedEvent;
import org.c4.userservice.user.events.UserStatusUpdatedEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.security.Principal;
import java.util.UUID;

import static org.c4.userservice.user.mapping.UserMappingValue.PRIVATE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

class UserEventServiceTest
{
    private final String USER_ID = UUID.randomUUID().toString();

    private EventPublisher   eventPublisher;
    private UserEventService userEventService;

    @BeforeEach
    void setUp()
    {
        eventPublisher = mock(EventPublisher.class);
        userEventService = new UserEventService(eventPublisher);
    }

    @Test
    void dispatchUserMappingCreatedEvent()
    {
        UserIdDto userIdDto = new UserIdDto();
        userIdDto.setId("id");
        Principal p = new BasicUserPrincipal("creator");

        userEventService.dispatchUserCreatedEvent(userIdDto, p);

        ArgumentCaptor<UserCreatedEvent> createdEvent = ArgumentCaptor.forClass(
                UserCreatedEvent.class);

        verify(eventPublisher, times(1)).send(createdEvent.capture());
        assertThat(createdEvent.getValue().getAggregate().getUserId(), is(equalTo("id")));
        assertThat(createdEvent.getValue().getCreatedBy(), is(equalTo("creator")));
    }

    /**
     * Beim Versenden eines {@link UserStatusUpdatedEvent}s werden die korrekten Werte übermittelt.
     */
    @Test
    void dispatchUserStatusUpdatedEvent()
    {
        UserEntity userEntity = createUserEntity();

        UserStatusChangeDto changeDto = new UserStatusChangeDto();
        changeDto.setUserStatus(UserStatus.INACTIVE);

        UserIdDto userIdDto = UserIdDto.of(USER_ID);

        userEventService.dispatchUserStatusUpdatedEvent(userEntity, changeDto, userIdDto);

        ArgumentCaptor<UserStatusUpdatedEvent> argumentCaptor = ArgumentCaptor.forClass(
                UserStatusUpdatedEvent.class);

        verify(eventPublisher, times(1)).send(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getChangeDto(), is(equalTo(changeDto)));
        assertThat(argumentCaptor.getValue().getAggregateId(),
                is(equalTo(userEntity.getAggregateId())));
        assertThat(argumentCaptor.getValue().getVersion(), is(equalTo(userEntity.getVersion())));
        assertThat(argumentCaptor.getValue().getCreatedBy(), is(equalTo(USER_ID)));
    }

    private UserEntity createUserEntity()
    {
        UserEntity userEntity = new UserEntity();
        userEntity.setVersion(1L);
        userEntity.setUserPersistenceDto(createUserPersistenceDto());

        return userEntity;
    }

    private UserPersistenceDto createUserPersistenceDto()
    {
        UserPersistenceDto userPersistenceDto = new UserPersistenceDto();
        userPersistenceDto.setUserId(USER_ID);
        userPersistenceDto.setEmail(PRIVATE);
        userPersistenceDto.setUserType(PRIVATE);
        userPersistenceDto.setUserStatus(UserStatus.ACTIVE);
        userPersistenceDto.setUsername(PRIVATE);

        return userPersistenceDto;
    }
}
