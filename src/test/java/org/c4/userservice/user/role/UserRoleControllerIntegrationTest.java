package org.c4.userservice.user.role;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.c4.userservice.ErrorKey;
import org.c4.userservice.user.role.data.UserRole;
import org.c4.userservice.user.role.data.UserRoleDataDto;
import org.c4.userservice.user.role.data.UserRoleRestDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integrationtest für den {@link UserRoleController}.
 * <br/>
 * Copyright: Copyright (c) 07.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class UserRoleControllerIntegrationTest extends KeycloakMockTestSetup
{
    private static final String ROLES_FOR_USER      = "/users/{id}/roles";
    private static final String GET_AVAILABLE_ROLES = "/users/roles";

    @Autowired
    private MockMvc      mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void getRolesForUser_success() throws Exception
    {
        identityProviderSuccess = true;

        mockMvc.perform(get(ROLES_FOR_USER, USER_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result.roles[*]", contains(UserRole.USER_ADMIN.name())));
    }

    /**
     * Ein Fehler im IdentityProvider wird korrekt verarbeitet.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void getRolesForUser_failure() throws Exception
    {
        identityProviderSuccess = false;

        mockMvc.perform(get(ROLES_FOR_USER, USER_ID))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.errors[*]", contains(ErrorKey.USER_NOT_FOUND)));
    }

    /**
     * Es werden alle möglichen Rollen zurückgeliefert.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void getAvailableRoles() throws Exception
    {
        identityProviderSuccess = true;

        mockMvc.perform(get(GET_AVAILABLE_ROLES))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result.roles[*]", containsInAnyOrder(
                        Arrays.stream(UserRole.values()).map(UserRole::name).toArray())));
    }

    /**
     * Beim Hinzufügen einer Rolle wird das Repo korrekt aufgerufen und der Status wird korrekt zurückgegeben.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void addRolesToUser_success() throws Exception
    {
        identityProviderSuccess = true;

        mockMvc.perform(put(ROLES_FOR_USER, USER_ID).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(new UserRoleRestDto(
                        Collections.singletonList(UserRole.USER_ADMIN.name())))))
                .andExpect(status().isOk());

        verify(identityProviderRepository, times(1)).addRoleToUser(any(UserRoleDataDto.class));
    }

    /**
     * Tritt bei der Verarbeitung ein Fehler auf, wird dieser korrekt propagiert.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void addRolesToUser_failure() throws Exception
    {
        identityProviderSuccess = false;

        mockMvc.perform(put(ROLES_FOR_USER, USER_ID).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(new UserRoleRestDto(
                        Collections.singletonList(UserRole.USER_ADMIN.name())))))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[*]").isNotEmpty());
    }

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void removeRolesFromUser_success() throws Exception
    {
        identityProviderSuccess = true;

        mockMvc.perform(delete(ROLES_FOR_USER, USER_ID).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(new UserRoleRestDto(
                        Collections.singletonList(UserRole.USER_ADMIN.name())))))
                .andExpect(status().isOk());

        verify(identityProviderRepository, times(1)).removeRoleFromUser(any(UserRoleDataDto.class));
    }

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void removeRolesFromUser_failure() throws Exception
    {
        identityProviderSuccess = false;

        mockMvc.perform(put(ROLES_FOR_USER, USER_ID).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(new UserRoleRestDto(
                        Collections.singletonList(UserRole.USER_ADMIN.name())))))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[*]").isNotEmpty());
    }
}
