package org.c4.userservice.user.role;

import org.c4.userservice.ErrorKey;
import org.c4.microservice.framework.communication.ExtendedProcessingResult;
import org.c4.microservice.framework.communication.ProcessingResult;
import org.c4.microservice.framework.communication.VoidProcessingResult;
import org.c4.userservice.identityprovider.IdentityProviderRepository;
import org.c4.userservice.identityprovider.UserProcessingState;
import org.c4.userservice.user.data.UserDataDto;
import org.c4.userservice.user.data.UserIdDto;
import org.c4.userservice.user.role.data.UserRole;
import org.c4.userservice.user.role.data.UserRoleDataDto;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.c4.userservice.identityprovider.UserProcessingState.CREATED;
import static org.c4.userservice.identityprovider.UserProcessingState.ERROR;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;

/**
 * Oberklasse für Tests, die auf den KeycloakIdentityProvider zugreifen müssen.
 * <br/>
 * Copyright: Copyright (c) 07.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@SpringBootTest
@AutoConfigureMockMvc
public abstract class KeycloakMockTestSetup
{
    protected final String USER_ID = UUID.randomUUID().toString();

    @MockBean
    protected IdentityProviderRepository identityProviderRepository;

    protected boolean identityProviderSuccess;

    @BeforeEach
    void setUp()
    {
        setupCreateUser();
        setupGetUserById();
        setupGetUserRoles();
        addOrRemoveRoleToUser();
    }

    private void setupCreateUser()
    {
        ExtendedProcessingResult<UserIdDto, UserProcessingState> successResult = new ExtendedProcessingResult<>();
        successResult.setProcessingState(CREATED);
        successResult.setResult(new UserIdDto(USER_ID));

        ExtendedProcessingResult<UserIdDto, UserProcessingState> erroneousResult = new ExtendedProcessingResult<>();
        erroneousResult.setProcessingState(ERROR);
        erroneousResult.addError(ErrorKey.UNDEFINED_KEYCLOAK_ERROR);

        doAnswer(invocation -> identityProviderSuccess ? successResult : erroneousResult).when(
                identityProviderRepository).createUser(any());
    }

    private void setupGetUserById()
    {
        UserDataDto dataDto = new UserDataDto();
        dataDto.setUserId(USER_ID);

        doAnswer(invocation -> identityProviderSuccess ? Optional.of(dataDto) :
                Optional.empty()).when(identityProviderRepository).getUserById(USER_ID);
    }

    private void setupGetUserRoles()
    {
        UserRoleDataDto dataDto = new UserRoleDataDto();
        dataDto.setUserId(USER_ID);
        dataDto.setRoles(Collections.singletonList(UserRole.USER_ADMIN));

        ProcessingResult<UserRoleDataDto> errorResult = new ProcessingResult<>();
        errorResult.addError(ErrorKey.USER_NOT_FOUND);

        doAnswer(invocation -> identityProviderSuccess ? ProcessingResult.of(dataDto) : errorResult)
                .when(identityProviderRepository)
                .getUserRoles(USER_ID);
    }

    private void addOrRemoveRoleToUser()
    {
        VoidProcessingResult errorResult = new VoidProcessingResult();
        errorResult.addError(ErrorKey.UNDEFINED_KEYCLOAK_ERROR);

        doAnswer(invocation -> identityProviderSuccess ? new VoidProcessingResult() :
                errorResult).when(identityProviderRepository).addRoleToUser(any());
        doAnswer(invocation -> identityProviderSuccess ? new VoidProcessingResult() :
                errorResult).when(identityProviderRepository).removeRoleFromUser(any());
    }
}
