package org.c4.userservice.user;

import org.apache.http.auth.BasicUserPrincipal;
import org.c4.microservice.framework.communication.ExtendedProcessingResult;
import org.c4.microservice.framework.communication.ProcessingResult;
import org.c4.microservice.framework.communication.VoidProcessingResult;
import org.c4.userservice.ErrorKey;
import org.c4.userservice.identityprovider.IdentityProviderRepository;
import org.c4.userservice.identityprovider.UserProcessingState;
import org.c4.userservice.user.data.*;
import org.c4.userservice.user.persistence.UserRepository;
import org.c4.userservice.user.role.data.UserRole;
import org.c4.userservice.user.role.data.UserRoleDataDto;
import org.c4.userservice.user.role.data.UserRoleRestDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.security.Principal;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.c4.userservice.identityprovider.UserProcessingState.*;
import static org.c4.userservice.user.data.UserType.MEMBER;
import static org.c4.userservice.user.mapping.UserMappingValue.PRIVATE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Test für {@link UserService}.
 * <br/>
 * Copyright: Copyright (c) 17.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class UserServiceTest
{
    private final String USER_ID  = UUID.randomUUID().toString();
    private final String USERNAME = "username";
    private final String EMAIL    = "email";

    private IdentityProviderRepository identityProviderRepository = mock(
            IdentityProviderRepository.class);
    private UserEventService           userEventService           = mock(UserEventService.class);
    private UserRepository             userRepository             = mock(UserRepository.class);
    private UserService                userService                = new UserService(
            identityProviderRepository, userEventService, userRepository);
    private Principal                  principal                  = new BasicUserPrincipal(
            "createdBy");

    @BeforeEach
    void setUp()
    {
    }

    /**
     * Im Erfolgsfall meldet das zugrundeliegende Repo Erfolg. Ein entsprechendes Event wird gesendet.
     */
    @Test
    void createUser_success()
    {
        ExtendedProcessingResult<UserIdDto, UserProcessingState> keycloakResult = createProcessingResult(
                USER_ID, CREATED);
        doReturn(keycloakResult).when(identityProviderRepository).createUser(any());

        UserCreateRestDto restDto = createRestDto();

        ProcessingResult<UserIdDto> result = userService.createUser(restDto, principal);

        assertThat(result, is(equalTo(keycloakResult)));
        verify(userEventService, times(1)).dispatchUserCreatedEvent(any(), eq(principal));
    }

    /**
     * Sind Mail oder Username bereits belegt, wird ein CONFLICT zurückgegeben.
     */
    @Test
    void createUser_conflict()
    {
        ExtendedProcessingResult<UserIdDto, UserProcessingState> keycloakResult = createProcessingResult(
                null, CONFLICT);
        doReturn(keycloakResult).when(identityProviderRepository).createUser(any());

        UserCreateRestDto restDto = createRestDto();

        ProcessingResult<UserIdDto> result = userService.createUser(restDto, principal);

        assertThat(result, is(equalTo(keycloakResult)));
        verify(userEventService, never()).dispatchUserCreatedEvent(any(), eq(principal));
    }

    /**
     * Sollte es auf Seiten von Keycloak zu einem Fehler kommen, wird dies entsprechend weitergegeben.
     */
    @Test
    void createUser_error()
    {
        ExtendedProcessingResult<UserIdDto, UserProcessingState> keycloakResult = createProcessingResult(
                null, ERROR);
        doReturn(keycloakResult).when(identityProviderRepository).createUser(any());

        UserCreateRestDto restDto = createRestDto();

        ProcessingResult<UserIdDto> result = userService.createUser(restDto, principal);

        assertThat(result, is(equalTo(keycloakResult)));
        verify(userEventService, never()).dispatchUserCreatedEvent(any(), eq(principal));
    }

    @Test
    void updateUser_identityProviderCalled()
    {
        UserManagedUpdateRestDto restDto = new UserManagedUpdateRestDto();
        restDto.setUserType(MEMBER.name());

        userService.updateUserManaged(new UserIdDto(USER_ID), restDto);

        verify(identityProviderRepository, times(1)).updateUserManaged(any(), any());
    }

    /**
     * User wird korrekt ermittelt und in ein RestDto umgewandelt.
     */
    @Test
    void getUserById_success()
    {
        UserDataDto dataDto = createUser();
        doReturn(Optional.of(dataDto)).when(identityProviderRepository).getUserById(USER_ID);

        Optional<UserRestDto> restDto = userService.getUserById(USER_ID);

        assertTrue(restDto.isPresent());
        assertThat(restDto.get().getEmail(), is(equalTo(EMAIL)));
        assertThat(restDto.get().getUsername(), is(equalTo(USERNAME)));
    }

    /**
     * Kann der User nicht gefunden werden, wird ein leeres Optional zurückgegeben.
     */
    @Test
    void getUserById_notFound()
    {
        UserDataDto dataDto = createUser();
        doReturn(Optional.of(dataDto)).when(identityProviderRepository).getUserById(USER_ID);

        Optional<UserRestDto> restDto = userService.getUserById(UUID.randomUUID().toString());

        assertFalse(restDto.isPresent());
    }

    /**
     * UserRollen werden korrekt zurückgegeben.
     */
    @Test
    void getUserRoles_success()
    {
        UserRoleDataDto dataDto = createUserRoleDataDto();
        doReturn(ProcessingResult.of(dataDto)).when(identityProviderRepository)
                .getUserRoles(USER_ID);

        ProcessingResult<UserRoleRestDto> result = userService.getUserRoles(new UserIdDto(USER_ID));

        assertThat(result.isValid(), is(equalTo(true)));
        assertThat(result.getResult().getRoles(), contains(UserRole.USER_ADMIN.toString()));
    }

    /**
     * Wenn kein Resultat vorliegt, werden die Fehler nach oben propagiert.
     */
    @Test
    void getUserRoles_noResult()
    {
        ProcessingResult<UserRoleDataDto> dataDtoProcessingResult = new ProcessingResult<>();
        dataDtoProcessingResult.addError(ErrorKey.EMAIL_USER_MAPPING_INVALID);
        doReturn(dataDtoProcessingResult).when(identityProviderRepository).getUserRoles(USER_ID);
        ProcessingResult<UserRoleRestDto> result = userService.getUserRoles(new UserIdDto(USER_ID));

        assertThat(result.isValid(), is(equalTo(false)));
        assertThat(result.getErrors(), contains(ErrorKey.EMAIL_USER_MAPPING_INVALID));
    }

    /**
     * Type wird für den User korrekt ermittelt.
     */
    @Test
    void getTypeForUser_success()
    {
        UserDataDto dataDto = createUser();
        doReturn(Optional.of(dataDto)).when(identityProviderRepository).getUserById(USER_ID);

        ProcessingResult<UserType> result = userService.getTypeForUser(USER_ID);

        assertThat(result.isValid(), is(equalTo(true)));
        assertThat(result.getResult(), is(equalTo(MEMBER)));
    }

    /**
     * Wird der User nicht gefunden, wird dies entsprechend zurückgegeben.
     */
    @Test
    void getTypeForUser_userNotFound()
    {
        UserDataDto dataDto = createUser();
        doReturn(Optional.empty()).when(identityProviderRepository).getUserById(USER_ID);

        Optional<UserRestDto> restDto = userService.getUserById(USER_ID);

        ProcessingResult<UserType> result = userService.getTypeForUser(USER_ID);

        assertThat(result.isValid(), is(equalTo(false)));
        assertThat(result.getErrors(), contains(ErrorKey.USER_NOT_FOUND));
    }

    /**
     * Wenn beim Setzen der Rolle eines Users ein Fehler auftritt, wird dieser korrekt propagiert.
     */
    @Test
    void addRoleToUser_errorsArePropagated()
    {
        VoidProcessingResult processingResult = new VoidProcessingResult();
        processingResult.addError(ErrorKey.ROLE_NOT_FOUND);
        doReturn(processingResult).when(identityProviderRepository).addRoleToUser(any());

        VoidProcessingResult result = userService.addRolesToUser(new UserIdDto(USER_ID),
                new UserRoleRestDto(Collections.singletonList("USER_ADMIN")));

        assertThat(result.isValid(), is(equalTo(false)));
        assertThat(result.getErrors(), contains(ErrorKey.ROLE_NOT_FOUND));
    }

    /**
     * Wenn beim Löschen der Rolle eines Users ein Fehler auftritt, wird dieser korrekt propagiert.
     */
    @Test
    void removeRoleFromUser_errorsArePropagated()
    {
        VoidProcessingResult processingResult = new VoidProcessingResult();
        processingResult.addError(ErrorKey.ROLE_NOT_FOUND);
        doReturn(processingResult).when(identityProviderRepository).removeRoleFromUser(any());

        VoidProcessingResult result = userService.removeRolesFromUser(new UserIdDto(USER_ID),
                new UserRoleRestDto(Collections.singletonList("USER_ADMIN")));

        assertThat(result.isValid(), is(equalTo(false)));
        assertThat(result.getErrors(), contains(ErrorKey.ROLE_NOT_FOUND));
    }

    /**
     * UserStatus wird korrekt gesetzt.
     */
    @Test
    void updateUserStatus_success()
    {
        UserEntity userEntity = createUserEntity();
        doReturn(Optional.of(userEntity)).when(userRepository).findByUserId(USER_ID);

        UserStatusRestDto restDto = new UserStatusRestDto();
        restDto.setUserStatus(UserStatus.INACTIVE.name());

        VoidProcessingResult voidProcessingResult = userService.updateUserStatus(
                UserIdDto.of(USER_ID), restDto, UserIdDto.of(USER_ID));

        ArgumentCaptor<UserStatusChangeDto> argumentCaptor = ArgumentCaptor.forClass(
                UserStatusChangeDto.class);
        verify(userEventService, times(1)).dispatchUserStatusUpdatedEvent(eq(userEntity),
                argumentCaptor.capture(), any());

        assertThat(argumentCaptor.getValue().getUserStatus(), is(equalTo(UserStatus.INACTIVE)));
        assertThat(voidProcessingResult.isValid(), is(equalTo(true)));
    }

    /**
     * UserStatus wird nicht gesetzt, weil der User nicht gefunden wurde.
     */
    @Test
    void updateUserStatus_userNotFound()
    {
        doReturn(Optional.empty()).when(userRepository).findByUserId(USER_ID);

        UserStatusRestDto restDto = new UserStatusRestDto();
        restDto.setUserStatus(UserStatus.INACTIVE.name());

        VoidProcessingResult voidProcessingResult = userService.updateUserStatus(
                UserIdDto.of(USER_ID), restDto, UserIdDto.of(USER_ID));

        verify(userEventService, never()).dispatchUserStatusUpdatedEvent(any(), any(), any());

        assertThat(voidProcessingResult.isValid(), is(equalTo(false)));
        assertThat(voidProcessingResult.getErrors(), contains(ErrorKey.USER_NOT_FOUND));
    }

    /**
     * UserStatus wird nicht gesetzt, weil das Update keine Änderung herbeiführt.
     */
    @Test
    void updateUserStatus_updateIdentical()
    {
        UserEntity userEntity = createUserEntity();
        doReturn(Optional.of(userEntity)).when(userRepository).findByUserId(USER_ID);

        UserStatusRestDto restDto = new UserStatusRestDto();
        restDto.setUserStatus(UserStatus.ACTIVE.name());

        VoidProcessingResult voidProcessingResult = userService.updateUserStatus(
                UserIdDto.of(USER_ID), restDto, UserIdDto.of(USER_ID));

        verify(userEventService, never()).dispatchUserStatusUpdatedEvent(any(), any(), any());

        assertThat(voidProcessingResult.isValid(), is(equalTo(false)));
        assertThat(voidProcessingResult.getErrors(), contains(ErrorKey.UPDATE_IDENTICAL));
    }

    private ExtendedProcessingResult<UserIdDto, UserProcessingState> createProcessingResult(
            String userId, UserProcessingState processingState)
    {
        ExtendedProcessingResult<UserIdDto, UserProcessingState> result = new ExtendedProcessingResult<>();
        result.setResult(userId == null ? null : new UserIdDto(userId));
        result.setProcessingState(processingState);

        return result;
    }

    private UserRoleDataDto createUserRoleDataDto()
    {
        UserRoleDataDto dataDto = new UserRoleDataDto();
        dataDto.setUserId(USER_ID);
        dataDto.setRoles(Collections.singletonList(UserRole.USER_ADMIN));

        return dataDto;
    }

    private UserCreateRestDto createRestDto()
    {
        UserCreateRestDto restDto = new UserCreateRestDto();
        restDto.setEmail(EMAIL);
        restDto.setUsername(USERNAME);

        return restDto;
    }

    private UserDataDto createUser()
    {
        UserDataDto dataDto = new UserDataDto();
        dataDto.setUserId(USER_ID);
        dataDto.setUsername(USERNAME);
        dataDto.setEmail(EMAIL);
        dataDto.setUserType(MEMBER);

        return dataDto;
    }

    private UserEntity createUserEntity()
    {
        UserEntity userEntity = new UserEntity();
        userEntity.setVersion(1L);
        userEntity.setUserPersistenceDto(createUserPersistenceDto());

        return userEntity;
    }

    private UserPersistenceDto createUserPersistenceDto()
    {
        UserPersistenceDto userPersistenceDto = new UserPersistenceDto();
        userPersistenceDto.setUserId(USER_ID);
        userPersistenceDto.setEmail(PRIVATE);
        userPersistenceDto.setUserType(PRIVATE);
        userPersistenceDto.setUserStatus(UserStatus.ACTIVE);
        userPersistenceDto.setUsername(PRIVATE);

        return userPersistenceDto;
    }
}
