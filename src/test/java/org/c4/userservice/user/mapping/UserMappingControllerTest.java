package org.c4.userservice.user.mapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.c4.userservice.ErrorKey;
import org.c4.microservice.framework.domainevent.DomainEventSink;
import org.c4.microservice.framework.domainevent.communication.DomainEvent;
import org.c4.microservice.framework.domainevent.messaging.EventPublisher;
import org.c4.userservice.user.data.UserEntity;
import org.c4.userservice.user.data.UserPersistenceDto;
import org.c4.userservice.user.mapping.data.UserMappingRestDto;
import org.c4.userservice.user.mapping.events.UserMappingUpdatedEvent;
import org.c4.userservice.user.persistence.SpringDataUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;
import java.util.UUID;

import static org.c4.userservice.user.mapping.UserMappingValue.AUTHENTICATED;
import static org.c4.userservice.user.mapping.UserMappingValue.PRIVATE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Testet die korrekte Funktionalität des {@link UserMappingController}s.
 * <br/>
 * Copyright: Copyright (c) 20.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@SpringBootTest
@AutoConfigureMockMvc
class UserMappingControllerTest
{
    private final String AGGREGATE_ID = UUID.randomUUID().toString();
    private final String USER_ID      = UUID.randomUUID().toString();

    private final String UPDATE_MAPPING_URL = "/users/{id}/mapping";

    @Autowired
    private MockMvc      mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private EventPublisher           eventPublisher;
    @Autowired
    private DomainEventSink          domainEventSink;
    @Autowired
    private SpringDataUserRepository springDataUserRepository;

    @BeforeEach
    void setUp()
    {
        doAnswer(invocation -> {
            DomainEvent domainEvent = invocation.getArgument(0);
            domainEvent.setVersion(
                    domainEvent.getVersion() == null ? 0L : domainEvent.getVersion() + 1);
            domainEventSink.consumeDomainEvent(invocation.getArgument(0));
            return null;
        }).when(eventPublisher).send(any());

        springDataUserRepository.deleteAll();
    }

    /**
     * Wenn es kein UserMapping zu einem User gibt, wird ein entsprechender Fehler zurückgegeben.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "noroleuser")
    void updateOwnUserMapping_notFound() throws Exception
    {
        UserMappingRestDto restDto = createUserMappingRestDto();
        mockMvc.perform(put(UPDATE_MAPPING_URL, "self").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(restDto)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", contains(ErrorKey.USER_NOT_FOUND)));
    }

    /**
     * Wenn es kein UserMapping zu einem User gibt, wird ein entsprechender Fehler zurückgegeben.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "noroleuser")
    void updateOwnUserMapping_ok() throws Exception
    {
        UserEntity persistence = createPersistence();
        persistence.getUserPersistenceDto().setUserId("noroleuser");
        springDataUserRepository.save(persistence);

        UserMappingRestDto restDto = createUserMappingRestDto();
        mockMvc.perform(put(UPDATE_MAPPING_URL, "self").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(restDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.errors").doesNotExist());

        verify(eventPublisher, times(1)).send(any(UserMappingUpdatedEvent.class));

        Optional<UserEntity> result = springDataUserRepository.findByUserPersistenceDto_userId(
                "noroleuser");

        assertTrue(result.isPresent());
        assertThat(result.get().getUserPersistenceDto().getEmail(), is(equalTo(PRIVATE)));
        assertThat(result.get().getVersion(), is(equalTo(1L)));
    }

    /**
     * Wenn es zu dem User kein Mapping gibt, wird dieser Fehler entsprechend zurückgegeben.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void updateOtherUserMapping_notFound() throws Exception
    {
        UserMappingRestDto restDto = createUserMappingRestDto();
        mockMvc.perform(put(UPDATE_MAPPING_URL, USER_ID).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(restDto)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", contains(ErrorKey.USER_NOT_FOUND)));
    }

    /**
     * UserMapping eines anderen Users wird korrekt geändert.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "admin")
    void updateOtherUserMapping_ok() throws Exception
    {
        UserEntity persistence = createPersistence();
        springDataUserRepository.save(persistence);

        UserMappingRestDto restDto = createUserMappingRestDto();
        mockMvc.perform(put(UPDATE_MAPPING_URL, USER_ID).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(restDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.errors").doesNotExist());

        verify(eventPublisher, times(1)).send(any(UserMappingUpdatedEvent.class));

        Optional<UserEntity> result = springDataUserRepository.findByUserPersistenceDto_userId(
                USER_ID);

        assertTrue(result.isPresent());
        assertThat(result.get().getUserPersistenceDto().getEmail(), is(equalTo(PRIVATE)));
        assertThat(result.get().getVersion(), is(equalTo(1L)));
    }

    private UserMappingRestDto createUserMappingRestDto()
    {
        UserMappingRestDto restDto = new UserMappingRestDto();
        restDto.setEmail("PRIVATE");
        restDto.setUserType("AUTHENTICATED");

        return restDto;
    }

    private UserEntity createPersistence()
    {
        UserPersistenceDto dataDto = new UserPersistenceDto();
        dataDto.setUserId(USER_ID);
        dataDto.setUsername(AUTHENTICATED);
        dataDto.setEmail(AUTHENTICATED);

        UserEntity persistence = new UserEntity();
        persistence.setAggregateId(AGGREGATE_ID);
        persistence.setVersion(0L);
        persistence.setUserPersistenceDto(dataDto);

        return persistence;
    }
}
