package org.c4.userservice.user.persistence;

import org.bson.types.ObjectId;
import org.c4.userservice.user.data.UserEntity;
import org.c4.userservice.user.data.UserPersistenceDto;
import org.c4.userservice.user.mapping.UserMappingValue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

/**
 * Testet die korrekte Funktionalität der Mongo-Implementation des {@link UserRepository}s.
 * <br/>
 * Copyright: Copyright (c) 16.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class MongoUserRepositoryImplTest
{
    private SpringDataUserRepository springDataUserRepository;

    private UserRepository userRepository;

    @BeforeEach
    void setUp()
    {
        springDataUserRepository = mock(SpringDataUserRepository.class);
        userRepository = new MongoUserRepositoryImpl(springDataUserRepository);
    }

    @Test
    void save()
    {
        UserPersistenceDto userPersistenceDto = createUserMappingDto();
        UserEntity persistence = createUserMappingPersistence(userPersistenceDto);

        userRepository.save(persistence);

        verify(springDataUserRepository, times(1)).save(persistence);
    }

    private UserPersistenceDto createUserMappingDto()
    {
        UserPersistenceDto userPersistenceDto = new UserPersistenceDto();
        userPersistenceDto.setUserId("userId");
        userPersistenceDto.setEmail(UserMappingValue.PRIVATE);

        return userPersistenceDto;
    }

    private UserEntity createUserMappingPersistence(UserPersistenceDto dataDto)
    {
        UserEntity persistence = new UserEntity();
        persistence.setUserPersistenceDto(dataDto);
        persistence.setAggregateId(new ObjectId().toHexString());
        persistence.setVersion(0L);

        return persistence;
    }
}
