package org.c4.userservice.identityprovider;

import org.junit.jupiter.api.Test;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

/**
 * Verifikation, dass der {@link KeycloakAttributeBuilder} korrekte Ergebnisse liefert.
 * <br/>
 * Copyright: Copyright (c) 10.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class KeycloakAttributeBuilderTest
{
    private final String       KEY   = "key";
    private final List<String> VALUE = Collections.singletonList("value");

    /**
     * Liefert eine leere Map zurück.
     */
    @Test
    void empty()
    {
        Map<String, List<String>> result = KeycloakAttributeBuilder.empty().build();

        assertNotNull(result);
        assertThat(result.isEmpty(), is(equalTo(true)));
    }

    /**
     * Liefert die übergebene Map zurück.
     */
    @Test
    void ofMap()
    {
        Map<String, List<String>> input = new HashMap<>();
        input.put(KEY, VALUE);

        Map<String, List<String>> result = KeycloakAttributeBuilder.of(input).build();

        assertNotNull(result);
        assertThat(result.isEmpty(), is(equalTo(false)));
        assertThat(result.get(KEY), is(equalTo(VALUE)));
    }

    /**
     * Extrahier die Attribute eines Users.
     */
    @Test
    void ofUserResource_withAttributes()
    {
        UserResource userResource = createUserResource(true);

        Map<String, List<String>> result = KeycloakAttributeBuilder.of(userResource).build();

        assertNotNull(result);
        assertThat(result.isEmpty(), is(equalTo(false)));
    }

    @Test
    void ofUserResource_noAttributes()
    {
        UserResource userResource = createUserResource(false);

        Map<String, List<String>> result = KeycloakAttributeBuilder.of(userResource).build();

        assertNotNull(result);
        assertThat(result.isEmpty(), is(equalTo(true)));
    }

    @Test
    void ofUserRepresentation_withAttributes()
    {
        UserRepresentation userRepresentation = createUserRepresentation(true);

        Map<String, List<String>> result = KeycloakAttributeBuilder.of(userRepresentation).build();

        assertNotNull(result);
        assertThat(result.isEmpty(), is(equalTo(false)));
    }

    @Test
    void ofUserRepresentation_noAttributes()
    {
        UserRepresentation userRepresentation = createUserRepresentation(false);

        Map<String, List<String>> result = KeycloakAttributeBuilder.of(userRepresentation).build();

        assertNotNull(result);
        assertThat(result.isEmpty(), is(equalTo(true)));
    }

    @Test
    void ofKeyValue()
    {
        Map<String, List<String>> result = KeycloakAttributeBuilder.of(KEY, "value").build();

        assertNotNull(result);
        assertThat(result.get(KEY).get(0), is(equalTo("value")));
    }

    @Test
    void ofKeyValueList()
    {
        Map<String, List<String>> result = KeycloakAttributeBuilder.of(KEY, VALUE).build();

        assertNotNull(result);
        assertThat(result.get(KEY), is(equalTo(VALUE)));
    }

    @Test
    void addKey_new()
    {
        Map<String, List<String>> result = KeycloakAttributeBuilder.of(KEY, VALUE)
                .addKey("key2", "value2")
                .build();

        assertNotNull(result);
        assertThat(result.size(), is(equalTo(2)));
        assertThat(result.get("key2"), is(equalTo(Collections.singletonList("value2"))));
    }

    @Test
    void addKey_overwriteOld()
    {
        Map<String, List<String>> result = KeycloakAttributeBuilder.of(KEY, VALUE)
                .addKey(KEY, "value2")
                .build();

        assertNotNull(result);
        assertThat(result.size(), is(equalTo(1)));
        assertThat(result.get(KEY), is(equalTo(Collections.singletonList("value2"))));
    }

    @Test
    void addKeyList_new()
    {
        Map<String, List<String>> result = KeycloakAttributeBuilder.of(KEY, VALUE)
                .addKey("key2", Collections.singletonList("value2"))
                .build();

        assertNotNull(result);
        assertThat(result.size(), is(equalTo(2)));
        assertThat(result.get("key2"), is(equalTo(Collections.singletonList("value2"))));
    }

    @Test
    void addKeyList_overwriteOld()
    {
        Map<String, List<String>> result = KeycloakAttributeBuilder.of(KEY, VALUE)
                .addKey(KEY, Collections.singletonList("value2"))
                .build();

        assertNotNull(result);
        assertThat(result.size(), is(equalTo(1)));
        assertThat(result.get(KEY), is(equalTo(Collections.singletonList("value2"))));
    }

    @Test
    void addToKey()
    {
        Map<String, List<String>> result = KeycloakAttributeBuilder.of(KEY, VALUE)
                .addToKey(KEY, "value2")
                .build();

        assertNotNull(result);
        assertThat(result.size(), is(equalTo(1)));
        assertThat(result.get(KEY), containsInAnyOrder(VALUE.get(0), "value2"));
    }

    @Test
    void addToKeyList()
    {
        Map<String, List<String>> result = KeycloakAttributeBuilder.of(KEY, VALUE)
                .addToKey(KEY, Collections.singletonList("value2"))
                .build();

        assertNotNull(result);
        assertThat(result.size(), is(equalTo(1)));
        assertThat(result.get(KEY), containsInAnyOrder(VALUE.get(0), "value2"));
    }

    @Test
    void removeKey()
    {
        Map<String, List<String>> result = KeycloakAttributeBuilder.of(KEY, VALUE)
                .removeKey(KEY)
                .build();

        assertNotNull(result);
        assertThat(result.isEmpty(), is(equalTo(true)));
    }

    @Test
    void removeFromKey_emptyRemoved()
    {
        Map<String, List<String>> result = KeycloakAttributeBuilder.of(KEY, VALUE)
                .addKey("key2", "value2")
                .removeFromKey(KEY, VALUE.get(0))
                .build();

        assertNotNull(result);
        assertThat(result.size(), is(equalTo(1)));
        assertNull(result.get(KEY));
    }

    @Test
    void removeFromKey()
    {
        Map<String, List<String>> result = KeycloakAttributeBuilder.of(KEY, VALUE)
                .addToKey(KEY, "value2")
                .removeFromKey(KEY, VALUE.get(0))
                .build();

        assertNotNull(result);
        assertThat(result.size(), is(equalTo(1)));
        assertNotNull(result.get(KEY));
        assertThat(result.get(KEY), is(equalTo(Collections.singletonList("value2"))));
    }

    @Test
    void removeFromKeyList()
    {
        Map<String, List<String>> result = KeycloakAttributeBuilder.of(KEY, VALUE)
                .addToKey(KEY, "value2")
                .removeFromKey(KEY, Collections.singletonList(VALUE.get(0)))
                .build();

        assertNotNull(result);
        assertThat(result.size(), is(equalTo(1)));
        assertNotNull(result.get(KEY));
        assertThat(result.get(KEY), is(equalTo(Collections.singletonList("value2"))));
    }

    private UserResource createUserResource(boolean withAttributes)
    {
        UserResource userResource = mock(UserResource.class);
        doReturn(createUserRepresentation(withAttributes)).when(userResource).toRepresentation();

        return userResource;
    }

    private UserRepresentation createUserRepresentation(boolean withAttributes)
    {
        UserRepresentation userRepresentation = new UserRepresentation();
        if (withAttributes)
        {
            Map<String, List<String>> attributeMap = new HashMap<>();
            attributeMap.put(KEY, VALUE);
            userRepresentation.setAttributes(attributeMap);
        }

        return userRepresentation;
    }
}
