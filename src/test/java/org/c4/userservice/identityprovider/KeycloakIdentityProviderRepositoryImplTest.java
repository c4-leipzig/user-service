package org.c4.userservice.identityprovider;

import org.bson.types.ObjectId;
import org.c4.userservice.ErrorKey;
import org.c4.microservice.framework.communication.ExtendedProcessingResult;
import org.c4.microservice.framework.communication.ProcessingResult;
import org.c4.microservice.framework.communication.VoidProcessingResult;
import org.c4.userservice.user.data.*;
import org.c4.userservice.user.role.data.UserRole;
import org.c4.userservice.user.role.data.UserRoleDataDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.keycloak.admin.client.resource.*;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.mockito.ArgumentCaptor;

import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Testet die Funktionalität der Keycloak-Anbindung.
 * <br/>
 * Copyright: Copyright (c) 07.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class KeycloakIdentityProviderRepositoryImplTest
{
    private final String USER_ID  = "userId";
    private final String USERNAME = "user";
    private final String EMAIL    = "user@email.de";

    private final String ROLE_USER_ADMIN         = "USER_ADMIN";
    private final String ROLE_UMA                = "uma_authorization";
    private final String USER_TYPE_ATTRIBUTE_KEY = "userType";
    IdentityProviderRepository    identityProviderRepository;
    KeycloakUserConversionService userConversionService = new KeycloakUserConversionService(
            USER_TYPE_ATTRIBUTE_KEY);
    private UsersResource usersResource;
    private RolesResource rolesResource;

    @BeforeEach
    void setUp()
    {
        RealmResource realmResource = mock(RealmResource.class);
        usersResource = mock(UsersResource.class);
        rolesResource = mock(RolesResource.class);
        doReturn(usersResource).when(realmResource).users();
        doReturn(rolesResource).when(realmResource).roles();

        identityProviderRepository = new KeycloakIdentityProviderRepositoryImpl(realmResource,
                userConversionService);
    }

    /**
     * Keycloak wird korrekt aufgerufen.
     */
    @Test
    void createUser_success()
    {
        ArgumentCaptor<UserRepresentation> submittedUser = ArgumentCaptor.forClass(
                UserRepresentation.class);
        UserCreateRestDto restDto = createRestDto();
        Response response = Response.created(URI.create("asdf")).build();
        doReturn(response).when(usersResource).create(any());

        ExtendedProcessingResult<UserIdDto, UserProcessingState> result = identityProviderRepository
                .createUser(restDto);

        verify(usersResource, times(1)).create(submittedUser.capture());
        assertTrue(result.hasProcessingState(UserProcessingState.CREATED));
        assertThat(submittedUser.getValue().getUsername(), is(equalTo(USERNAME)));
        assertThat(submittedUser.getValue().getEmail(), is(equalTo(EMAIL)));
    }

    /**
     * Keycloak wird korrekt aufgerufen. Ein Fehler durch Keycloak wird korrekt propagiert.
     */
    @Test
    void createUser_conflict()
    {
        ArgumentCaptor<UserRepresentation> submittedUser = ArgumentCaptor.forClass(
                UserRepresentation.class);
        UserCreateRestDto restDto = createRestDto();
        Response response = Response.status(Response.Status.CONFLICT).build();
        doReturn(response).when(usersResource).create(any());

        ExtendedProcessingResult<UserIdDto, UserProcessingState> result = identityProviderRepository
                .createUser(restDto);

        verify(usersResource, times(1)).create(submittedUser.capture());
        assertTrue(result.hasProcessingState(UserProcessingState.CONFLICT));
        assertThat(submittedUser.getValue().getUsername(), is(equalTo(USERNAME)));
        assertThat(submittedUser.getValue().getEmail(), is(equalTo(EMAIL)));
    }

    /**
     * Keycloak wird korrekt aufgerufen. Ein Fehler durch Keycloak wird korrekt propagiert.
     */
    @Test
    void createUser_failure()
    {
        ArgumentCaptor<UserRepresentation> submittedUser = ArgumentCaptor.forClass(
                UserRepresentation.class);
        UserCreateRestDto restDto = createRestDto();
        Response response = Response.status(Response.Status.BAD_REQUEST).build();
        doReturn(response).when(usersResource).create(any());

        ExtendedProcessingResult<UserIdDto, UserProcessingState> result = identityProviderRepository
                .createUser(restDto);

        verify(usersResource, times(1)).create(submittedUser.capture());
        assertTrue(result.hasProcessingState(UserProcessingState.ERROR));
        assertThat(submittedUser.getValue().getUsername(), is(equalTo(USERNAME)));
        assertThat(submittedUser.getValue().getEmail(), is(equalTo(EMAIL)));
    }

    /**
     * Keycloak wird korrekt angefragt, Ergebnis wird korrekt zurückgegeben.
     */
    @Test
    void updateUser_success()
    {
        UserResource userResource = createUserResource(true);
        doReturn(createUserRepresentation()).when(userResource).toRepresentation();
        doReturn(userResource).when(usersResource).get(USER_ID);

        UserManagedUpdateRestDto restDto = new UserManagedUpdateRestDto();
        restDto.setUserType(UserType.HONORARY_MEMBER.name());

        identityProviderRepository.updateUserManaged(new UserIdDto(USER_ID), restDto);

        ArgumentCaptor<UserRepresentation> argumentCaptor = ArgumentCaptor.forClass(
                UserRepresentation.class);
        verify(userResource, times(1)).update(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getAttributes().get(USER_TYPE_ATTRIBUTE_KEY),
                is(equalTo(Collections.singletonList(UserType.HONORARY_MEMBER.name()))));
    }

    /**
     * Wird der User nicht gefunden, wird dies korrekt propagiert.
     */
    @Test
    void updateUser_notFound()
    {
        doReturn(null).when(usersResource).get(USER_ID);

        UserManagedUpdateRestDto restDto = new UserManagedUpdateRestDto();
        restDto.setUserType(UserType.HONORARY_MEMBER.name());

        VoidProcessingResult result = identityProviderRepository.updateUserManaged(
                new UserIdDto(USER_ID), restDto);

        assertThat(result.getErrors(), contains(ErrorKey.USER_NOT_FOUND));
    }

    /**
     * Tritt beim Updaten eines Users ein Fehler auf, wird dies korrekt gemeldet.
     */
    @Test
    void updateUser_failure()
    {
        UserResource userResource = createUserResource(true);
        doReturn(createUserRepresentation()).when(userResource).toRepresentation();
        doAnswer(invocation -> {
            throw new Exception();
        }).when(userResource).update(any());
        doReturn(userResource).when(usersResource).get(USER_ID);

        UserManagedUpdateRestDto restDto = new UserManagedUpdateRestDto();
        restDto.setUserType(UserType.HONORARY_MEMBER.name());

        VoidProcessingResult result = identityProviderRepository.updateUserManaged(
                new UserIdDto(USER_ID), restDto);

        ArgumentCaptor<UserRepresentation> argumentCaptor = ArgumentCaptor.forClass(
                UserRepresentation.class);
        verify(userResource, times(1)).update(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getAttributes().get(USER_TYPE_ATTRIBUTE_KEY),
                is(equalTo(Collections.singletonList(UserType.HONORARY_MEMBER.name()))));
        assertThat(result.getErrors(), contains(ErrorKey.UNDEFINED_KEYCLOAK_ERROR));
    }

    /**
     * User wird korrekt abgefragt.
     */
    @Test
    void getUserById_success()
    {
        UserResource userResource = mock(UserResource.class);
        UserRepresentation user = createUserRepresentation();
        doReturn(userResource).when(usersResource).get(anyString());
        doReturn(user).when(userResource).toRepresentation();

        Optional<UserDataDto> result = identityProviderRepository.getUserById(USER_ID);

        assertTrue(result.isPresent());
        assertThat(result.get().getEmail(), is(equalTo(EMAIL)));
        assertThat(result.get().getUsername(), is(equalTo(USERNAME)));
    }

    /**
     * Wird kein User gefunden, wird ein leeres Optional zurückgegeben.
     */
    @Test
    void getUserById_notFound()
    {
        doReturn(null).when(usersResource).get(anyString());

        Optional<UserDataDto> result = identityProviderRepository.getUserById(USER_ID);

        assertFalse(result.isPresent());
    }

    /**
     * Bei der Abfrage des Users tritt ein Fehler auf.
     */
    @Test
    void getUserById_failure()
    {
        doAnswer(invocation -> {
            throw new Exception();
        }).when(usersResource).get(anyString());

        Optional<UserDataDto> result = identityProviderRepository.getUserById(USER_ID);

        assertFalse(result.isPresent());
    }

    /**
     * Userrollen werden korrekt ermittelt.
     * Eingebaute Keycloak-Rollen werden rausgefiltert.
     */
    @Test
    void getUserRoles_success()
    {
        UserResource userResource = createUserResource(true);
        doReturn(userResource).when(usersResource).get(USER_ID);

        ProcessingResult<UserRoleDataDto> result = identityProviderRepository.getUserRoles(USER_ID);

        assertThat(result.isValid(), is(equalTo(true)));
        assertThat(result.getResult().getRoles().size(), is(equalTo(1)));
        assertThat(result.getResult().getRoles(), contains(UserRole.USER_ADMIN));
    }

    /**
     * Wenn der User nicht gefunden wird, wird dies korrekt propagiert.
     */
    @Test
    void getUserRoles_userNotFound()
    {
        doReturn(null).when(usersResource).get(USER_ID);

        ProcessingResult<UserRoleDataDto> result = identityProviderRepository.getUserRoles(USER_ID);

        assertThat(result.isValid(), is(equalTo(false)));
        assertThat(result.getErrors(), contains(ErrorKey.USER_NOT_FOUND));
    }

    /**
     * Sind dem User keine Rollen zugeordnet, wird dies korrekt zurückgegeben.
     */
    @Test
    void getUserRoles_noRolesOnUser()
    {
        doReturn(createUserResource(false)).when(usersResource).get(USER_ID);

        ProcessingResult<UserRoleDataDto> result = identityProviderRepository.getUserRoles(USER_ID);

        assertThat(result.isValid(), is(equalTo(true)));
        assertThat(result.getResult().getRoles().isEmpty(), is(equalTo(true)));
    }

    /**
     * Rollen werden korrekt dem User hinzugefügt.
     */
    @Test
    void addRoleToUser_success()
    {
        List<RoleRepresentation> roles = Collections.singletonList(
                createRoleRepresentation(ROLE_USER_ADMIN));
        doReturn(roles).when(rolesResource).list();
        doReturn(createUserResource(true)).when(usersResource).get(USER_ID);

        VoidProcessingResult result = identityProviderRepository.addRoleToUser(
                createUserRoleDataDto());

        assertThat(result.isErrorFree(), is(equalTo(true)));
    }

    /**
     * Wenn der User nicht gefunden wird, wird dies korrekt propagiert.
     */
    @Test
    void addRoleToUser_userNotFound()
    {
        List<RoleRepresentation> roles = Collections.singletonList(
                createRoleRepresentation(ROLE_USER_ADMIN));
        doReturn(roles).when(rolesResource).list();
        doReturn(null).when(usersResource).get(USER_ID);

        VoidProcessingResult result = identityProviderRepository.addRoleToUser(
                createUserRoleDataDto());

        assertThat(result.isErrorFree(), is(equalTo(false)));
        assertThat(result.getErrors(), contains(ErrorKey.USER_NOT_FOUND));
    }

    /**
     * Rollen werden korrekt entfernt.
     */
    @Test
    void removeRoleFromUser_success()
    {
        List<RoleRepresentation> roles = Collections.singletonList(
                createRoleRepresentation(ROLE_USER_ADMIN));
        doReturn(roles).when(rolesResource).list();
        doReturn(createUserResource(true)).when(usersResource).get(USER_ID);

        VoidProcessingResult result = identityProviderRepository.removeRoleFromUser(
                createUserRoleDataDto());

        assertThat(result.isErrorFree(), is(equalTo(true)));
    }

    /**
     * Wenn der User nicht gefunden wird, wird dies korrekt propagiert.
     */
    @Test
    void removeRoleFromUser_userNotFound()
    {
        List<RoleRepresentation> roles = Collections.singletonList(
                createRoleRepresentation(ROLE_USER_ADMIN));
        doReturn(roles).when(rolesResource).list();
        doReturn(null).when(usersResource).get(USER_ID);

        VoidProcessingResult result = identityProviderRepository.removeRoleFromUser(
                createUserRoleDataDto());

        assertThat(result.isErrorFree(), is(equalTo(false)));
        assertThat(result.getErrors(), contains(ErrorKey.USER_NOT_FOUND));
    }

    private UserRoleDataDto createUserRoleDataDto()
    {
        UserRoleDataDto dataDto = new UserRoleDataDto();
        dataDto.setUserId(USER_ID);
        dataDto.setRoles(Collections.singletonList(UserRole.USER_ADMIN));

        return dataDto;
    }

    private UserCreateRestDto createRestDto()
    {
        UserCreateRestDto restDto = new UserCreateRestDto();
        restDto.setUsername(USERNAME);
        restDto.setEmail(EMAIL);

        return restDto;
    }

    private UserRepresentation createUserRepresentation()
    {
        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setUsername(USERNAME);
        userRepresentation.setEmail(EMAIL);
        userRepresentation.setId(USER_ID);
        userRepresentation.setAttributes(
                KeycloakAttributeBuilder.of(USER_TYPE_ATTRIBUTE_KEY, UserType.MEMBER.name())
                        .build());

        return userRepresentation;
    }

    private RoleRepresentation createRoleRepresentation(String name)
    {
        RoleRepresentation roleRepresentation = new RoleRepresentation();
        roleRepresentation.setName(name);
        roleRepresentation.setId(new ObjectId().toHexString());

        return roleRepresentation;
    }

    private UserResource createUserResource(boolean withRoles)
    {
        UserResource userResource = mock(UserResource.class);
        RoleMappingResource roleMappingResource = mock(RoleMappingResource.class);
        RoleScopeResource roleScopeResource = mock(RoleScopeResource.class);

        RoleRepresentation userAdmin = new RoleRepresentation();
        userAdmin.setName(ROLE_USER_ADMIN);
        RoleRepresentation umaAuthorization = new RoleRepresentation();
        umaAuthorization.setName(ROLE_UMA);

        doReturn(roleMappingResource).when(userResource).roles();
        doReturn(roleScopeResource).when(roleMappingResource).realmLevel();
        if (withRoles)
        {
            doReturn(Arrays.asList(userAdmin, umaAuthorization)).when(roleScopeResource).listAll();
        }
        else
        {
            doReturn(null).when(roleScopeResource).listAll();
        }

        return userResource;
    }
}
