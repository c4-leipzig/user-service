package org.c4.userservice.domainevent;

import org.bson.types.ObjectId;
import org.c4.microservice.framework.domainevent.repository.DomainEventRepository;
import org.c4.userservice.user.data.UserEntity;
import org.c4.userservice.user.data.UserPersistenceDto;
import org.c4.userservice.user.events.UserCreatedEvent;
import org.c4.userservice.user.persistence.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.time.Instant;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Testet die korrekte Funktionalität des {@link DomainEventHandlingContext}es.
 * <br/>
 * Copyright: Copyright (c) 16.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class DomainEventHandlingContextTest
{
    private OngoingDomainEventHandlingContextImpl domainEventHandlingContext;

    @Mock
    private UserRepository        userRepository;
    @Mock
    private DomainEventRepository domainEventRepository;

    @BeforeEach
    void setUp()
    {
        initMocks(this);
        domainEventHandlingContext = new OngoingDomainEventHandlingContextImpl(userRepository,
                domainEventRepository);
    }

    /**
     * getVersion bezieht die Information aus dem AggregateRepository.
     */
    @Test
    void getUserMappingAggregate_callsRepo()
    {
        UserCreatedEvent domainEvent = createDomainEvent();

        domainEventHandlingContext.getUserAggregate(domainEvent.getAggregateId());

        verify(userRepository, times(1)).findByAggregateId(domainEvent.getAggregateId());
    }

    /**
     * Beim Neuladen eines Aggregats wird das domainEventRepository abgefragt
     * und die zurückgelieferten Events werden verarbeitet.
     */
    @Test
    void reloadAggregate()
    {
        List<UserCreatedEvent> domainEvents = Collections.singletonList(createDomainEvent());
        doReturn(domainEvents).when(domainEventRepository).getEventStream("aggregateId", 1L);

        domainEventHandlingContext.reloadAggregate("aggregateId", 1L);

        verify(domainEventRepository, times(1)).getEventStream("aggregateId", 1L);
        verify(userRepository, times(1)).save(any());
    }

    /**
     * Bei der Verarbeitung eines UserMappingCreatedEvents wird das Aggregat korrekt persistiert.
     */
    @Test
    void handleUserMappingCreatedEvent()
    {
        UserCreatedEvent domainEvent = createDomainEvent();
        UserPersistenceDto aggregate = new UserPersistenceDto();
        domainEvent.setAggregate(aggregate);
        UserEntity userEntity = createUserMappingPersistence(aggregate);

        domainEventHandlingContext.handleUserEvent(userEntity);

        ArgumentCaptor<UserEntity> argumentCaptor = ArgumentCaptor.forClass(UserEntity.class);
        verify(userRepository, times(1)).save(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getUserPersistenceDto(), is(equalTo(aggregate)));
    }

    private UserCreatedEvent createDomainEvent()
    {
        UserCreatedEvent userCreatedEvent = new UserCreatedEvent();
        userCreatedEvent.setAggregateId(new ObjectId().toHexString());
        userCreatedEvent.setVersion(1L);
        return userCreatedEvent;
    }

    private UserEntity createUserMappingPersistence(UserPersistenceDto dataDto)
    {
        UserEntity userEntity = new UserEntity();
        userEntity.setAggregateId(new ObjectId().toHexString());
        userEntity.setLastModifiedAt(Instant.now());
        userEntity.setLastModifiedBy("lastModifiedBy");
        userEntity.setVersion(0L);
        userEntity.setUserPersistenceDto(dataDto);

        return userEntity;
    }
}
