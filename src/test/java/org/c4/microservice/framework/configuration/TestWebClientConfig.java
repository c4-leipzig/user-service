package org.c4.microservice.framework.configuration;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@Profile("test")
@Log4j2
public class TestWebClientConfig
{
    @Bean
    WebClient webClient()
    {
        return WebClient.builder().filter(((request, next) -> {
            log.debug("[{}] {}", request::method, request::url);
            return next.exchange(request);
        })).build();
    }
}
