package org.c4.microservice.framework.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.Arrays;
import java.util.Collections;

/**
 * Mockt Users für Tests.
 * <br/>
 * Copyright: Copyright (c) 16.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
public class MockUserDetailsService
{
    @Bean
    @Primary
    public UserDetailsService userDetailsService()
    {
        User backendUser = new User("backend", "xxx",
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_backend")));

        User adminUser = new User("admin", "xxx",
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER_ADMIN")));

        User noRoleUser = new User("noroleuser", "xxx", Collections.emptyList());

        return new InMemoryUserDetailsManager(Arrays.asList(backendUser, adminUser, noRoleUser));
    }
}
