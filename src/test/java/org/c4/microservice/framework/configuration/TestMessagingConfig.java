package org.c4.microservice.framework.configuration;

import org.c4.microservice.framework.domainevent.messaging.EventPublisher;
import org.c4.microservice.framework.domainevent.messaging.MockEventPublisher;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Konfiguration zur Überbrückung des Kafkas für Tests.
 * Relevante Schnittstellen ({@link EventPublisher}) werden gemockt.
 * <br/>
 * Copyright: Copyright (c) 22.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
@Profile("test")
@EnableAutoConfiguration(exclude = KafkaAutoConfiguration.class)
public class TestMessagingConfig
{
    @Bean
    public EventPublisher eventPublisher()
    {
        return new MockEventPublisher();
    }
}
