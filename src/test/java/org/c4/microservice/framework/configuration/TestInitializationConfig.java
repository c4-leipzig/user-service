package org.c4.microservice.framework.configuration;

import org.c4.userservice.domainevent.DomainEventHandlingContext;
import org.c4.microservice.framework.domainevent.repository.DomainEventRepository;
import org.c4.microservice.framework.initialization.InitializationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class TestInitializationConfig
{
    @Bean
    @Profile("test")
    InitializationRunner initializationRunner(DomainEventRepository domainEventRepository,
            DomainEventHandlingContext initialDomainEventHandlingContextImpl)
    {
        return new InitializationRunner(domainEventRepository,
                initialDomainEventHandlingContextImpl);
    }
}
