package org.c4.microservice.framework.domainevent.communication;

import org.bson.types.ObjectId;
import org.c4.userservice.domainevent.DomainEventHandlingContext;
import org.c4.microservice.framework.domainevent.DomainEventProcessingState;
import org.c4.userservice.user.data.UserEntity;
import org.c4.userservice.user.events.UserCreatedEvent;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.c4.microservice.framework.domainevent.DomainEventProcessingState.OBSOLETE;
import static org.c4.microservice.framework.domainevent.DomainEventProcessingState.PROCESSED;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Testet die korrekte Verarbeitung con {@link CreateDomainEvent}s.
 * <br/>
 * Copyright: Copyright (c) 19.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class CreateDomainEventTest
{
    DomainEventHandlingContext handlingContext = mock(DomainEventHandlingContext.class);

    /**
     * Außerhalb des Initialisierungskontextes wird jedes Create-Event sofort verarbeitet.
     */
    @Test
    void handleEvent_notInitializing_processed()
    {
        doReturn(false).when(handlingContext).isInitializing();

        CreateDomainEvent createdEvent = createDomainEvent();

        DomainEventProcessingState result = createdEvent.handleEvent(handlingContext);

        assertThat(result, is(equalTo(PROCESSED)));
        verify(handlingContext, times(1)).handleUserEvent(any());
    }

    /**
     * Im Initialisierungskontext wird ein Event nicht verarbeitet, wenn es bereits ein entsprechendes Aggregat gibt.
     */
    @Test
    void handleEvent_initializing_obsolete()
    {
        doReturn(true).when(handlingContext).isInitializing();
        doReturn(Optional.of(new UserEntity())).when(handlingContext).getUserAggregate(anyString());

        CreateDomainEvent createdEvent = createDomainEvent();
        DomainEventProcessingState result = createdEvent.handleEvent(handlingContext);

        assertThat(result, is(equalTo(OBSOLETE)));
        verify(handlingContext, never()).handleUserEvent(any());
    }

    /**
     * Im Initialisierungskontext wird das Event angewendet, wenn es noch kein entsprechendes Aggregat gibt.
     */
    @Test
    void handleEvent_initializing_processed()
    {
        doReturn(true).when(handlingContext).isInitializing();
        doReturn(Optional.empty()).when(handlingContext).getUserAggregate(anyString());

        CreateDomainEvent createdEvent = createDomainEvent();
        DomainEventProcessingState result = createdEvent.handleEvent(handlingContext);

        assertThat(result, is(equalTo(PROCESSED)));
        verify(handlingContext, times(1)).handleUserEvent(any());
    }

    private CreateDomainEvent createDomainEvent()
    {
        UserCreatedEvent createdEvent = new UserCreatedEvent();
        createdEvent.setAggregateId(new ObjectId().toHexString());
        createdEvent.setVersion(0L);

        return createdEvent;
    }
}
