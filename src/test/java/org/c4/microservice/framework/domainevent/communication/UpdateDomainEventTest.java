package org.c4.microservice.framework.domainevent.communication;

import org.bson.types.ObjectId;
import org.c4.userservice.domainevent.DomainEventHandlingContext;
import org.c4.microservice.framework.domainevent.DomainEventProcessingState;
import org.c4.userservice.user.data.UserEntity;
import org.c4.userservice.user.data.UserPersistenceDto;
import org.c4.userservice.user.mapping.data.UserMappingChangeDto;
import org.c4.userservice.user.mapping.events.UserMappingUpdatedEvent;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;

import static org.c4.microservice.framework.domainevent.DomainEventProcessingState.*;
import static org.c4.userservice.user.mapping.UserMappingValue.PRIVATE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Testet die korrekte Abarbeitung von {@link UpdateDomainEvent}s.
 * <br/>
 * Copyright: Copyright (c) 19.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class UpdateDomainEventTest
{
    private DomainEventHandlingContext handlingContext = mock(DomainEventHandlingContext.class);

    @Test
    void handleEvent_noAggregate_versionMismatch()
    {
        doReturn(Optional.empty()).when(handlingContext).getUserAggregate(anyString());

        UpdateDomainEvent domainEvent = createDomainEvent();

        DomainEventProcessingState result = domainEvent.handleEvent(handlingContext);

        assertThat(result, is(equalTo(VERSION_MISMATCH)));
        verify(handlingContext, never()).handleUserEvent(any());
        verify(handlingContext, times(1)).reloadAggregate(any(), any());
    }

    /**
     * Veraltete Events werden nicht angewendet.
     */
    @Test
    void handleEvent_obsolete()
    {
        UserEntity persistence = createUserEntity();
        doReturn(Optional.of(persistence)).when(handlingContext).getUserAggregate(anyString());

        UpdateDomainEvent domainEvent = createDomainEvent();
        domainEvent.setVersion(0L);

        DomainEventProcessingState result = domainEvent.handleEvent(handlingContext);

        assertThat(result, is(equalTo(OBSOLETE)));
        verify(handlingContext, never()).handleUserEvent(any());
    }

    /**
     * Wenn die aktuell vorgehaltene Version genau die Vorgängerversion des Events ist, wird dieses angewendet.
     */
    @Test
    void handleEvent_processed()
    {
        UserEntity userEntity = createUserEntity();
        doReturn(Optional.of(userEntity)).when(handlingContext).getUserAggregate(anyString());

        UserMappingUpdatedEvent domainEvent = createDomainEvent();
        domainEvent.setVersion(1L);

        DomainEventProcessingState result = domainEvent.handleEvent(handlingContext);

        assertThat(result, is(equalTo(PROCESSED)));
        verify(handlingContext, times(1)).handleUserEvent(any());
    }

    /**
     * Wenn zwischen dem aktuellen Stand und dem Event eine Lücke klafft, wird das Event nicht verarbeitet.
     * Stattdessen wird das Aggregat komplett neu vom Eventstore geladen.
     */
    @Test
    void handleEvent_missingEvent_versionMismatch()
    {
        UserEntity persistence = createUserEntity();
        doReturn(Optional.of(persistence)).when(handlingContext).getUserAggregate(anyString());

        UpdateDomainEvent domainEvent = createDomainEvent();
        domainEvent.setVersion(10L);

        DomainEventProcessingState result = domainEvent.handleEvent(handlingContext);

        assertThat(result, is(equalTo(VERSION_MISMATCH)));
        verify(handlingContext, never()).handleUserEvent(any());
        verify(handlingContext, times(1)).reloadAggregate(any(), any());
    }

    private UserMappingUpdatedEvent createDomainEvent()
    {
        UserMappingUpdatedEvent updatedEvent = new UserMappingUpdatedEvent();
        updatedEvent.setAggregateId(new ObjectId().toHexString());

        UserMappingChangeDto changeDto = new UserMappingChangeDto();
        changeDto.setEmail(PRIVATE);
        changeDto.setUserType(PRIVATE);
        updatedEvent.setAggregate(changeDto);

        return updatedEvent;
    }

    private UserEntity createUserEntity()
    {
        UserEntity persistence = new UserEntity();
        persistence.setVersion(0L);
        persistence.setUserPersistenceDto(createUserPersistenceDto());

        return persistence;
    }

    private UserPersistenceDto createUserPersistenceDto()
    {
        UserPersistenceDto userPersistenceDto = new UserPersistenceDto();
        userPersistenceDto.setUserId(UUID.randomUUID().toString());
        userPersistenceDto.setEmail(PRIVATE);
        userPersistenceDto.setUserType(PRIVATE);

        return userPersistenceDto;
    }
}
