package org.c4.microservice.framework.domainevent;

import org.c4.microservice.framework.domainevent.DomainEventSink;
import org.c4.microservice.framework.domainevent.KafkaAsyncDomainEventSinkImpl;
import org.c4.microservice.framework.domainevent.communication.DomainEvent;
import org.c4.userservice.domainevent.DomainEventHandlingContext;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

/**
 * Test für die Verarbeitung von Events durch die {@link DomainEventSink}.
 * <br/>
 * Copyright: Copyright (c) 15.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
class KafkaAsyncDomainEventSinkImplTest
{
    private KafkaAsyncDomainEventSinkImpl domainEventSink;

    private DomainEventHandlingContext handlingContext;

    /**
     * Sicherstellen, dass das Event verarbeitet wird.
     */
    @Test
    void consumeDomainEvent()
    {
        DomainEvent domainEvent = mock(DomainEvent.class);
        handlingContext = mock(DomainEventHandlingContext.class);
        domainEventSink = new KafkaAsyncDomainEventSinkImpl(handlingContext);

        domainEventSink.consumeDomainEvent(domainEvent);

        verify(domainEvent, times(1)).handleEvent(handlingContext);
    }
}
