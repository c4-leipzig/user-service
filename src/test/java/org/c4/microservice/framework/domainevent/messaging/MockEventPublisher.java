package org.c4.microservice.framework.domainevent.messaging;

import org.c4.microservice.framework.domainevent.communication.DomainEvent;
import org.c4.microservice.framework.domainevent.messaging.EventPublisher;

/**
 * Mock für den {@link EventPublisher}.
 * <br/>
 * Copyright: Copyright (c) 21.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class MockEventPublisher implements EventPublisher
{
    @Override
    public void send(DomainEvent domainEvent)
    {
        /*
         * Leere Methode, wird nur verwendet, um Kafka in Tests zu überbrücken.
         */
    }
}
