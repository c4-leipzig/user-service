package org.c4.microservice.framework.initialization;

import org.bson.types.ObjectId;
import org.c4.microservice.framework.domainevent.communication.DomainEvent;
import org.c4.microservice.framework.domainevent.repository.DomainEventRepository;
import org.c4.userservice.Application;
import org.c4.userservice.domainevent.DomainEventHandlingContext;
import org.c4.userservice.user.events.UserCreatedEvent;
import org.c4.userservice.user.mapping.events.UserMappingUpdatedEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Testet die korrekte initiale Aufdatung des lokalen Caches.
 * <br/>
 * Copyright: Copyright (c) 14.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@SpringBootTest(classes = { Application.class })
class InitializationRunnerTest
{
    @Mock
    DomainEventRepository domainEventRepository;

    @Mock
    DomainEventHandlingContext domainEventHandlingContext;

    private InitializationRunner initializationRunner;

    @BeforeEach
    void setUp()
    {
        initializationRunner = new InitializationRunner(domainEventRepository,
                domainEventHandlingContext);
    }

    /**
     * Der HandlingContext wird korrekt aufgerufen.
     */
    @Test
    void handleEventGetsCalled()
    {
        List<DomainEvent> domainEvents = createCreateEvents(5, 0L);
        doReturn(Collections.singletonList(domainEvents)).when(domainEventRepository)
                .getRelevantEvents(any());

        initializationRunner.run();

        verify(domainEventHandlingContext, times(5)).handleUserEvent(any());
    }

    /**
     * Bei einem VersionMismatch wird nur das erste Event betrachtet und nicht verarbeitet.
     */
    @Test
    void handleEvent_stopsOnVersionMismatch()
    {
        List<DomainEvent> domainEvents = createUpdateEvents(5, 5L);
        doReturn(Collections.singletonList(domainEvents)).when(domainEventRepository)
                .getRelevantEvents(any());

        initializationRunner.run();

        verify(domainEventHandlingContext, never()).handleUserEvent(any());
        verify(domainEventHandlingContext, times(1)).reloadAggregate(
                eq(domainEvents.get(0).getAggregateId()), anyLong());
    }

    /**
     * Wenn leere Listen zurückgeliefert werden, wird keine Exception geworfen.
     */
    @Test
    void handleEvent_emptyList()
    {
        List<List<DomainEvent>> domainEventLists = new ArrayList<>();
        domainEventLists.add(null);
        domainEventLists.add(Collections.emptyList());
        doReturn(domainEventLists).when(domainEventRepository).getRelevantEvents(any());

        assertDoesNotThrow(() -> initializationRunner.run());
        verify(domainEventHandlingContext, never()).handleUserEvent(any());
    }

    private List<DomainEvent> createCreateEvents(int amount, long version)
    {
        List<DomainEvent> domainEvents = new ArrayList<>();
        String id = new ObjectId().toHexString();
        for (int i = amount; i > 0; i--)
        {
            UserCreatedEvent createdEvent = new UserCreatedEvent();
            createdEvent.setVersion(version);
            createdEvent.setAggregateId(id);
            domainEvents.add(createdEvent);
        }
        return domainEvents;
    }

    private List<DomainEvent> createUpdateEvents(int amount, long version)
    {
        List<DomainEvent> domainEvents = new ArrayList<>();
        String id = new ObjectId().toHexString();
        for (int i = amount; i > 0; i--)
        {
            UserMappingUpdatedEvent updatedEvent = new UserMappingUpdatedEvent();
            updatedEvent.setVersion(version);
            updatedEvent.setAggregateId(id);
            domainEvents.add(updatedEvent);
        }
        return domainEvents;
    }
}
