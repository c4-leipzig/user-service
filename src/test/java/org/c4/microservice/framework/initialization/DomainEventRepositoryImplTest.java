package org.c4.microservice.framework.initialization;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.bson.types.ObjectId;
import org.c4.microservice.framework.communication.StringListDto;
import org.c4.microservice.framework.domainevent.communication.DomainEvent;
import org.c4.microservice.framework.domainevent.repository.DomainEventRepository;
import org.c4.userservice.Application;
import org.c4.userservice.user.events.UserCreatedEvent;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClientException;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test, dass DomainEvents korrekt vom Eventstore abgerufen werden.
 * <br/>
 * Copyright: Copyright (c) 14.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@SpringBootTest(classes = { Application.class })
class DomainEventRepositoryImplTest
{
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    private DomainEventRepository domainEventRepository;
    private MockWebServer mockWebServer;

    @BeforeEach
    void setUp() throws IOException
    {
        mockWebServer = new MockWebServer();
        mockWebServer.start(65001);
    }

    @AfterEach
    void tearDown() throws IOException
    {
        mockWebServer.shutdown();
    }

    /**
     * Testet, dass die korrekten Requests an den Eventstore gemacht werden.
     */
    @Test
    void getRelevantEvents() throws Exception
    {
        List<String> aggregateIds = Arrays.asList(new ObjectId().toHexString(),
                new ObjectId().toHexString());
        DomainEvent d1 = createDomainEvent(aggregateIds.get(0));
        DomainEvent d2 = createDomainEvent(aggregateIds.get(1));

        mockWebServer.enqueue(new MockResponse().setBody(
                objectMapper.writeValueAsString(new StringListDto(aggregateIds)))
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON));
        mockWebServer.enqueue(new MockResponse().setBody(objectMapper.writeValueAsString(d1))
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON));
        mockWebServer.enqueue(new MockResponse().setBody(objectMapper.writeValueAsString(d2))
                .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON));

        List<List<DomainEvent>> resultList = domainEventRepository.getRelevantEvents(
                Collections.singletonList("TestEvent"));

        assertNotNull(resultList);
        assertFalse(resultList.isEmpty());
        assertThat(resultList.size(), is(equalTo(2)));
        List<String> ids = resultList.stream()
                .map(domainEvents -> domainEvents.get(0).getAggregateId())
                .collect(Collectors.toList());
        assertThat(ids, contains(aggregateIds.get(0), aggregateIds.get(1)));
        assertThat(mockWebServer.getRequestCount(), is(equalTo(3)));
    }

    /**
     * Wenn der Eventstore nicht erreichbar ist, wird eine Exception geworfen.
     */
    @Test
    void getRelevantEvents_errorThrown()
    {
        mockWebServer.enqueue(new MockResponse().setResponseCode(400));

        assertThrows(WebClientException.class, () -> domainEventRepository.getRelevantEvents(
                Collections.singletonList("TestEvent")));
    }

    @Test
    void getRelevantEvents_noAggregateFoundReturnsEmptyList()
    {
        mockWebServer.enqueue(
                new MockResponse().setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON));

        List<List<DomainEvent>> resultList = domainEventRepository.getRelevantEvents(
                Collections.singletonList("TestEvent"));

        assertNotNull(resultList);
        assertTrue(resultList.isEmpty());
    }

    @Test
    void getRelevantEvents_404Permitted()
    {
        mockWebServer.enqueue(new MockResponse().setResponseCode(404));

        assertDoesNotThrow(() -> domainEventRepository.getRelevantEvents(
                Collections.singletonList("TestEvent")));
    }

    /**
     * EventStream wird korrekt zurückgegeben.
     */
    @Test
    void getEventStream() throws Exception
    {
        DomainEvent d1 = createDomainEvent(new ObjectId().toHexString());
        DomainEvent d2 = createDomainEvent(d1.getAggregateId());

        mockWebServer.enqueue(
                new MockResponse().setBody(objectMapper.writeValueAsString(Arrays.asList(d1, d2)))
                        .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON));

        List<DomainEvent> resultList = domainEventRepository.getEventStream(d1.getAggregateId());

        assertNotNull(resultList);
        assertFalse(resultList.isEmpty());
        assertThat(resultList.size(), is(equalTo(2)));
        assertThat(mockWebServer.getRequestCount(), is(equalTo(1)));
    }

    private DomainEvent createDomainEvent(String id)
    {
        DomainEvent domainEvent = new UserCreatedEvent();
        domainEvent.setAggregateId(id);

        return domainEvent;
    }
}
