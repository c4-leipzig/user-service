package org.c4.userservice.domainevent;

import lombok.extern.log4j.Log4j2;
import org.c4.microservice.framework.domainevent.repository.DomainEventRepository;
import org.c4.userservice.user.persistence.UserRepository;

/**
 * Implementation zur Verarbeitung von Events, die zur Laufzeit eingehen.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
public class OngoingDomainEventHandlingContextImpl extends DomainEventHandlingContext
{

    public OngoingDomainEventHandlingContextImpl(UserRepository userRepository,
            DomainEventRepository domainEventRepository)
    {
        super(userRepository, domainEventRepository);
    }

    @Override
    public boolean isInitializing()
    {
        return false;
    }
}
