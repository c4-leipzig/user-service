package org.c4.userservice.domainevent.initialization;

import lombok.extern.log4j.Log4j2;
import org.c4.microservice.framework.domainevent.repository.DomainEventRepository;
import org.c4.userservice.domainevent.DomainEventHandlingContext;
import org.c4.userservice.user.persistence.UserRepository;

/**
 * Implementation zur Verarbeitung von Events, die im Rahmen der initialen Aufdatung anfallen.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
public class InitialDomainEventHandlingContextImpl extends DomainEventHandlingContext
{
    public InitialDomainEventHandlingContextImpl(UserRepository userRepository,
            DomainEventRepository domainEventRepository)
    {
        super(userRepository, domainEventRepository);
    }

    @Override
    public boolean isInitializing()
    {
        return true;
    }
}
