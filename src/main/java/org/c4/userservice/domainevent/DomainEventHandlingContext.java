package org.c4.userservice.domainevent;

import lombok.extern.log4j.Log4j2;
import org.c4.microservice.framework.domainevent.repository.DomainEventRepository;
import org.c4.userservice.user.data.UserEntity;
import org.c4.userservice.user.events.UserCreatedEvent;
import org.c4.userservice.user.persistence.UserRepository;

import java.util.Optional;

/**
 * Context zur Verarbeitung eingehender Events.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
public abstract class DomainEventHandlingContext
{
    protected final UserRepository        userRepository;
    protected final DomainEventRepository domainEventRepository;

    protected DomainEventHandlingContext(UserRepository userRepository,
            DomainEventRepository domainEventRepository)
    {
        this.userRepository = userRepository;
        this.domainEventRepository = domainEventRepository;
    }

    /**
     * Gibt an, ob es sich um den regulären oder den Initialisierungskontext handelt.
     */
    public abstract boolean isInitializing();

    public void reloadAggregate(String aggregateId, Long minVersion)
    {
        domainEventRepository.getEventStream(aggregateId, minVersion)
                .forEach(domainEvent -> domainEvent.handleEvent(this));
    }

    /**
     * Funktion zur Verarbeitung von {@link UserCreatedEvent}s.
     */
    public void handleUserEvent(UserEntity userEntity)
    {
        userRepository.save(userEntity);
    }

    public Optional<UserEntity> getUserAggregate(String aggregateId)
    {
        return userRepository.findByAggregateId(aggregateId);
    }
}
