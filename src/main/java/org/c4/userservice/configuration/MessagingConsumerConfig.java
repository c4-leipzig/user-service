package org.c4.userservice.configuration;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.c4.microservice.framework.domainevent.KafkaAsyncDomainEventSinkImpl;
import org.c4.microservice.framework.domainevent.communication.DomainEvent;
import org.c4.microservice.framework.domainevent.messaging.EventPublisher;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

/**
 * Konfiguration für asynchrones Messaging mit Kafka.
 * Der {@link EventPublisher} veröffentlicht {@link DomainEvent}s, die {@link KafkaAsyncDomainEventSinkImpl} empfängt diese.
 * <br/>
 * Copyright: Copyright (c) 17.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
@Profile("!test")
public class MessagingConsumerConfig
{
    @Value("${spring.kafka.consumer.bootstrap-servers}")
    private String consumerBootstrapServers;
    @Value("${spring.kafka.consumer.group-id}")
    private String consumerGroupId;

    private Map<String, Object> consumerConfigs()
    {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, consumerBootstrapServers);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, consumerGroupId);

        return props;
    }

    @Bean
    public ConsumerFactory<String, DomainEvent> consumerFactory()
    {
        return new DefaultKafkaConsumerFactory<>(consumerConfigs(), new StringDeserializer(),
                new JsonDeserializer<>(DomainEvent.class));
    }
}
