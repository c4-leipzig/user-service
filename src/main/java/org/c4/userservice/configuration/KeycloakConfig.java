package org.c4.userservice.configuration;

import lombok.Getter;
import lombok.Setter;
import org.c4.userservice.identityprovider.IdentityProviderRepository;
import org.c4.userservice.identityprovider.KeycloakIdentityProviderRepositoryImpl;
import org.c4.userservice.identityprovider.KeycloakUserConversionService;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

/**
 * Konfiguration für den Keycloak-Admin-Client.
 * <br/>
 * Copyright: Copyright (c) 12.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
public class KeycloakConfig
{
    @Value("${keycloak.auth-server-url}")
    private String serverUrl;
    @Value("${keycloak.realm}")
    private String realm;
    @Value("${keycloak.resource}")
    private String clientId;
    @Value("${spring.security.oauth2.client.registration.keycloak.client-secret}")
    private String clientSecret;

    @Bean
    Keycloak keycloak()
    {
        return KeycloakBuilder.builder()
                .serverUrl(serverUrl)
                .realm(realm)
                .clientId(clientId)
                .clientSecret(clientSecret)
                .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
                .build();
    }

    @Bean
    RealmResource defaultRealmResource(Keycloak keycloak)
    {
        return keycloak.realm(realm);
    }

    @Bean
    IdentityProviderRepository identityProviderRepository(RealmResource realmResource,
            KeycloakUserConversionService keycloakUserConversionService)
    {
        return new KeycloakIdentityProviderRepositoryImpl(realmResource,
                keycloakUserConversionService);
    }

    @Bean
    KeycloakUserConversionService keycloakUserConversionService(
            UserConfigProperties userConfigProperties)
    {
        return new KeycloakUserConversionService(userConfigProperties.getUserTypeAttributeKey());
    }

    @Getter
    @Setter
    @Validated
    @Configuration
    @ConfigurationProperties(prefix = "c4.userservice.attribute-keys")
    public class UserConfigProperties
    {
        @NotBlank
        private String userTypeAttributeKey;
    }
}
