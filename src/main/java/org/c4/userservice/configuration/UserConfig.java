package org.c4.userservice.configuration;

import org.c4.microservice.framework.domainevent.messaging.EventPublisher;
import org.c4.userservice.identityprovider.IdentityProviderRepository;
import org.c4.userservice.user.UserEventService;
import org.c4.userservice.user.UserService;
import org.c4.userservice.user.persistence.MongoUserRepositoryImpl;
import org.c4.userservice.user.persistence.SpringDataUserRepository;
import org.c4.userservice.user.persistence.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Userkonfiguration.
 * <br/>
 * Copyright: Copyright (c) 15.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
public class UserConfig
{
    @Bean
    public UserEventService userEventService(EventPublisher eventPublisher)
    {
        return new UserEventService(eventPublisher);
    }

    @Bean
    public UserService userService(IdentityProviderRepository identityProviderRepository,
            UserEventService userEventService, UserRepository userRepository)
    {
        return new UserService(identityProviderRepository, userEventService, userRepository);
    }

    @Bean
    public UserRepository userRepository(SpringDataUserRepository springDataUserRepository)
    {
        return new MongoUserRepositoryImpl(springDataUserRepository);
    }
}
