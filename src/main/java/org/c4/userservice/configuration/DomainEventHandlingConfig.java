package org.c4.userservice.configuration;

import org.c4.microservice.framework.domainevent.DomainEventSink;
import org.c4.microservice.framework.domainevent.KafkaAsyncDomainEventSinkImpl;
import org.c4.microservice.framework.domainevent.repository.DomainEventRepository;
import org.c4.userservice.domainevent.DomainEventHandlingContext;
import org.c4.userservice.domainevent.OngoingDomainEventHandlingContextImpl;
import org.c4.userservice.domainevent.initialization.InitialDomainEventHandlingContextImpl;
import org.c4.userservice.user.persistence.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

/**
 * Config für das Verarbeiten von DomainEvents.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
public class DomainEventHandlingConfig
{
    @Bean
    public DomainEventHandlingContext ongoingDomainEventHandlingContextImpl(
            UserRepository userRepository, DomainEventRepository domainEventRepository)
    {
        return new OngoingDomainEventHandlingContextImpl(userRepository, domainEventRepository);
    }

    @Bean
    DomainEventHandlingContext initialDomainEventHandlingContextImpl(UserRepository userRepository,
            DomainEventRepository domainEventRepository)
    {
        return new InitialDomainEventHandlingContextImpl(userRepository, domainEventRepository);
    }

    // Die asynchrone DomainEventSink darf erst erstellt werden, wenn die Initialisierung abgeschlossen ist.
    @Bean
    @DependsOn(value = { "initializationRunner" })
    public DomainEventSink kafkaAsyncDomainEventSinkImpl(
            DomainEventHandlingContext ongoingDomainEventHandlingContextImpl)
    {
        return new KafkaAsyncDomainEventSinkImpl(ongoingDomainEventHandlingContextImpl);
    }
}
