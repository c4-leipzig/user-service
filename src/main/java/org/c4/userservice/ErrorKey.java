package org.c4.userservice;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Liste möglicher Fehler, die auftreten und an den User geschickt werden können.
 * Um die Werte statisch in Annotations verwenden zu können, wird auf die Verwendung einer Enum verzichtet.
 * <br/>
 * Copyright: Copyright (c) 12.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ErrorKey
{
    public static final String USERNAME_EMPTY           = "USERNAME_EMPTY";
    public static final String EMAIL_INVALID            = "EMAIL_INVALID";
    public static final String UNDEFINED_KEYCLOAK_ERROR = "UNDEFINED_KEYCLOAK_ERROR";
    public static final String USER_ID_INVALID          = "USER_ID_INVALID";

    public static final String USERNAME_OR_EMAIL_TAKEN = "USERNAME_OR_EMAIL_TAKEN";

    public static final String EMAIL_USER_MAPPING_INVALID     = "EMAIL_USER_MAPPING_INVALID";
    public static final String USER_TYPE_USER_MAPPING_INVALID = "USER_TYPE_USER_MAPPING_INVALID";

    public static final String USER_ROLES_INVALID    = "USER_ROLES_INVALID";
    public static final String USER_NOT_FOUND        = "USER_NOT_FOUND";
    public static final String ROLE_NOT_FOUND        = "ROLE_NOT_FOUND";
    public static final String USER_TYPE_NOT_FOUND   = "USER_TYPE_NOT_FOUND";
    public static final String USER_STATUS_NOT_FOUND = "USER_STATUS_NOT_FOUND";
    public static final String UPDATE_IDENTICAL      = "UPDATE_IDENTICAL";

    public static boolean isNotFoundError(String error)
    {
        return USER_NOT_FOUND.equals(error) || ROLE_NOT_FOUND.equals(error)
                || USER_TYPE_NOT_FOUND.equals(error) || USER_STATUS_NOT_FOUND.equals(error);
    }
}
