package org.c4.userservice.user.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.c4.microservice.framework.validation.UUIDValidator;
import org.c4.userservice.ErrorKey;

import javax.validation.constraints.Pattern;

/**
 * Repräsentation einer UserId.
 * <br/>
 * Copyright: Copyright (c) 12.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserIdDto
{
    @Pattern(regexp = UUIDValidator.UUID_PATTERN, message = ErrorKey.USER_ID_INVALID)
    private String id;

    public static UserIdDto of(String userId)
    {
        return new UserIdDto(userId);
    }
}
