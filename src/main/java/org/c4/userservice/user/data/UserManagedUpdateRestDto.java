package org.c4.userservice.user.data;

import lombok.Getter;
import lombok.Setter;
import org.c4.microservice.framework.validation.enums.EnumValue;
import org.c4.userservice.ErrorKey;

/**
 * Dto zur Übermittlung der Aktualisierung eines Users.
 * <br/>
 * Copyright: Copyright (c) 10.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
public class UserManagedUpdateRestDto
{
    @EnumValue(enumClazz = UserType.class, message = ErrorKey.USER_TYPE_NOT_FOUND)
    private String userType;
}
