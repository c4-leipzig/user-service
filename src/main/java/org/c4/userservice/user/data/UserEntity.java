package org.c4.userservice.user.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.c4.microservice.framework.communication.persistence.AbstractEntity;
import org.c4.microservice.framework.domainevent.communication.CreateDomainEvent;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Persistenzobjekt, das in der Datenbank gespeichert wird.
 * <br/>
 * Copyright: Copyright (c) 10.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@NoArgsConstructor
@Document(collection = "userMappings")
public class UserEntity extends AbstractEntity
{
    private UserPersistenceDto userPersistenceDto;

    public static UserEntity of(CreateDomainEvent domainEvent)
    {
        UserEntity persistence = new UserEntity();
        persistence.setAggregateId(domainEvent.getAggregateId());
        persistence.setCreatedAt(domainEvent.getCreatedAt());
        persistence.setCreatedBy(domainEvent.getCreatedBy());
        persistence.setLastModifiedAt(domainEvent.getCreatedAt());
        persistence.setLastModifiedBy(domainEvent.getCreatedBy());
        persistence.setVersion(domainEvent.getVersion());

        return persistence;
    }
}
