package org.c4.userservice.user.data;

/**
 * Klassifizierung eines Users in einen bestimmten Typus (Anwärter, Mitglied, ...).
 * <br/>
 * Copyright: Copyright (c) 09.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public enum UserType
{
    /**
     * Ein nicht weiter mit dem Club assoziierter Gast.
     */
    GUEST,
    /**
     * Ein Anwärter.
     */
    CANDIDATE,
    /**
     * Ein normales Mitglied des Vereins.
     */
    MEMBER,
    /**
     * Ein Ehrenmitglied des Vereins.
     */
    HONORARY_MEMBER,
    /**
     * Ein Mitglied der F11.
     */
    F11;

    public static UserType ofSave(String key)
    {
        return valueOf(key);
    }
}
