package org.c4.userservice.user.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.c4.userservice.user.mapping.UserMappingValue;

import static org.c4.userservice.user.data.UserStatus.ACTIVE;
import static org.c4.userservice.user.mapping.UserMappingValue.AUTHENTICATED;

/**
 * Darstellung eines UserMappings.
 * Hält für jedes Userattribut vor, ob es allgemein zugänglich sein soll.
 * <br/>
 * Copyright: Copyright (c) 15.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@NoArgsConstructor
public class UserPersistenceDto
{
    private String     userId;
    private UserStatus userStatus;

    private UserMappingValue username = AUTHENTICATED;
    private UserMappingValue email;
    private UserMappingValue userType;

    public static UserPersistenceDto initial(String userId)
    {
        UserPersistenceDto dataDto = new UserPersistenceDto();
        dataDto.setUserStatus(ACTIVE);
        dataDto.setUserId(userId);
        dataDto.setUsername(AUTHENTICATED);
        dataDto.setEmail(AUTHENTICATED);
        dataDto.setUserType(AUTHENTICATED);

        return dataDto;
    }
}
