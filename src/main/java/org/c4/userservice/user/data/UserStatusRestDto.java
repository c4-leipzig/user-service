package org.c4.userservice.user.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.c4.microservice.framework.validation.enums.EnumValue;
import org.c4.userservice.ErrorKey;

/**
 * RestDto zur Verarbeitung einer Statusänderung eines Users.
 * <br/>
 * Copyright: Copyright (c) 10.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@NoArgsConstructor
public class UserStatusRestDto
{
    @EnumValue(enumClazz = UserStatus.class, message = ErrorKey.USER_STATUS_NOT_FOUND)
    private String userStatus;
}
