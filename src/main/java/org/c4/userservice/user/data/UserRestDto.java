package org.c4.userservice.user.data;

import lombok.Getter;
import lombok.Setter;

/**
 * Externe Repräsentation eines Users.
 * <br/>
 * Copyright: Copyright (c) 14.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
public class UserRestDto
{
    private String userId;
    private String username;
    private String email;
    private String userType;
}
