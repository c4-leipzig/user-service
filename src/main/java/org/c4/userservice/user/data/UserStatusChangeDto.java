package org.c4.userservice.user.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.c4.microservice.framework.communication.delta.Delta;

import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
public class UserStatusChangeDto implements Delta<UserPersistenceDto>
{
    private UserStatus userStatus;

    @Override
    public UserPersistenceDto apply(UserPersistenceDto baseObject)
    {
        Optional.ofNullable(userStatus).ifPresent(baseObject::setUserStatus);
        return baseObject;
    }

    @Override
    public boolean isEmpty()
    {
        return userStatus == null;
    }
}
