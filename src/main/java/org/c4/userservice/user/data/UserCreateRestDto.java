package org.c4.userservice.user.data;

import lombok.Getter;
import lombok.Setter;
import org.c4.microservice.framework.validation.enums.EnumValue;
import org.c4.userservice.ErrorKey;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

/**
 * RestDto, über das ein neuer User angelegt wird.
 * <br/>
 * Copyright: Copyright (c) 09.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
public class UserCreateRestDto
{
    @NotEmpty(message = ErrorKey.USERNAME_EMPTY)
    private String username;

    @Email(message = ErrorKey.EMAIL_INVALID)
    private String email;

    @EnumValue(enumClazz = UserType.class, message = ErrorKey.USER_TYPE_NOT_FOUND)
    private String userType;
}
