package org.c4.userservice.user.data;

/**
 * Status eines Users.
 * <br/>
 * Copyright: Copyright (c) 10.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public enum UserStatus
{
    ACTIVE,
    INACTIVE,
    DELETED,
    LOCKED;

    public static UserStatus ofSave(String key)
    {
        return valueOf(key);
    }
}
