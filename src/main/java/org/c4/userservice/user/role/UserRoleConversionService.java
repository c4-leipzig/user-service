package org.c4.userservice.user.role;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.c4.userservice.user.data.UserIdDto;
import org.c4.userservice.user.role.data.UserRole;
import org.c4.userservice.user.role.data.UserRoleDataDto;
import org.c4.userservice.user.role.data.UserRoleRestDto;

import java.util.stream.Collectors;

/**
 * Umwandlung von UserRoleDtos.
 * <br/>
 * Copyright: Copyright (c) 20.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserRoleConversionService
{
    public static UserRoleRestDto transformToRestDto(UserRoleDataDto dataDto)
    {
        UserRoleRestDto restDto = new UserRoleRestDto();
        if (dataDto.getRoles() != null)
        {
            restDto.setRoles(
                    dataDto.getRoles().stream().map(UserRole::name).collect(Collectors.toList()));
        }

        return restDto;
    }

    public static UserRoleDataDto transformToDataDto(UserRoleRestDto restDto, UserIdDto id)
    {
        UserRoleDataDto dataDto = new UserRoleDataDto();
        dataDto.setUserId(id.getId());

        if (restDto.getRoles() != null)
        {
            dataDto.setRoles(
                    restDto.getRoles().stream().map(UserRole::ofSave).collect(Collectors.toList()));
        }

        return dataDto;
    }
}
