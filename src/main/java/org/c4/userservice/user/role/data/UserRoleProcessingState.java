package org.c4.userservice.user.role.data;

public enum UserRoleProcessingState
{
    RETRIEVED,
    USER_NOT_FOUND
}
