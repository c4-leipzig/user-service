package org.c4.userservice.user.role;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.c4.microservice.framework.communication.ProcessingResult;
import org.c4.microservice.framework.communication.ResponseEntityBuilder;
import org.c4.microservice.framework.communication.VoidProcessingResult;
import org.c4.userservice.user.UserService;
import org.c4.userservice.user.data.UserIdDto;
import org.c4.userservice.user.role.data.UserRole;
import org.c4.userservice.user.role.data.UserRoleRestDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.*;

/**
 * Controller zum Verwalten der Rollen, die ein User einnimmt.
 * <br/>
 * Copyright: Copyright (c) 20.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@RestController
@Tag(name = "User roles", description = "Endpoints for managing roles assigned to users.")
public class UserRoleController
{
    private final UserService userService;

    public UserRoleController(UserService userService)
    {
        this.userService = userService;
    }

    @GetMapping("users/{id}/roles")
    @PreAuthorize("hasRole('USER_ADMIN')")
    @ApiResponse(responseCode = "200", description = "Successful operation.")
    @ApiResponse(responseCode = "404", description = "User not found.")
    @Operation(summary = "Returns a list of roles that are assigned to this user.")
    public ResponseEntity<ProcessingResult<UserRoleRestDto>> getRolesForUser(
            @Valid UserIdDto userId)
    {
        ProcessingResult<UserRoleRestDto> result = userService.getUserRoles(userId);
        return ResponseEntity.status(result.isValid() ? OK : NOT_FOUND).body(result);
    }

    @GetMapping("users/roles")
    @PreAuthorize("hasRole('USER_ADMIN')")
    @ApiResponse(responseCode = "200", description = "Successful operation.")
    @Operation(summary = "Returns a list of all available roles.")
    public ResponseEntity<ProcessingResult<UserRoleRestDto>> getAvailableRoles()
    {
        return ResponseEntity.ok(ProcessingResult.of(new UserRoleRestDto(UserRole.getAllRoles())));
    }

    @PutMapping("users/{id}/roles")
    @PreAuthorize("hasRole('USER_ADMIN')")
    @ApiResponse(responseCode = "200", description = "Roles added successfully.")
    @ApiResponse(responseCode = "400", description = "Roles could not be added.")
    @Operation(summary = "Add a list of roles to a user.")
    public ResponseEntity<VoidProcessingResult> addRolesToUser(@Valid UserIdDto userId,
            @RequestBody @Valid UserRoleRestDto restDto)
    {
        return ResponseEntityBuilder.build(userService.addRolesToUser(userId, restDto), OK,
                BAD_REQUEST);
    }

    @DeleteMapping("users/{id}/roles")
    @PreAuthorize("hasRole('USER_ADMIN')")
    @ApiResponse(responseCode = "200", description = "Roles removed successfully.")
    @ApiResponse(responseCode = "400", description = "Roles could not be removed.")
    @Operation(summary = "Remove a list of roles from a user.")
    public ResponseEntity<VoidProcessingResult> removeRolesFromUser(@Valid UserIdDto userId,
            @RequestBody @Valid UserRoleRestDto restDto)
    {
        return ResponseEntityBuilder.build(userService.removeRolesFromUser(userId, restDto), OK,
                BAD_REQUEST);
    }
}
