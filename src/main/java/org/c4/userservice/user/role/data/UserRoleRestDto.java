package org.c4.userservice.user.role.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.c4.microservice.framework.validation.enums.EnumValues;
import org.c4.userservice.ErrorKey;

import java.util.ArrayList;
import java.util.List;

/**
 * RestDto zur Ein-/Ausgabe von UserRollen.
 * <br/>
 * Copyright: Copyright (c) 20.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRoleRestDto
{
    @EnumValues(enumClazz = UserRole.class, message = ErrorKey.USER_ROLES_INVALID, nullable = true)
    private List<String> roles = new ArrayList<>();
}
