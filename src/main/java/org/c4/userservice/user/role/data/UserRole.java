package org.c4.userservice.user.role.data;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Liste aller möglicher UserRollen.
 * <br/>
 * Copyright: Copyright (c) 20.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public enum UserRole
{
    USER_ADMIN,
    DEPARTMENT_ADMIN,
    CANDIDATE,
    ACTIVE_MEMBER,
    HONORARY_MEMBER;

    /**
     * Gibt alle Rollen als Liste aus.
     */
    public static List<String> getAllRoles()
    {
        return Arrays.stream(values()).map(UserRole::name).collect(Collectors.toList());
    }

    /**
     * Gibt die UserRole anhand ihres Namens zurück. Die Methode geht davon aus, dass nur gültige Werte
     * eingegeben werden!
     */
    public static UserRole ofSave(String saveUserRole)
    {
        return valueOf(saveUserRole);
    }
}
