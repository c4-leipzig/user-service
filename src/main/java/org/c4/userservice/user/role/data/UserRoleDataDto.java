package org.c4.userservice.user.role.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * DataDto zur internen Darstellung einer Liste von UserRoles, die einem User zugeordnet sind.
 * <br/>
 * Copyright: Copyright (c) 20.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@NoArgsConstructor
public class UserRoleDataDto
{
    private String userId;

    private List<UserRole> roles = new ArrayList<>();

    public List<String> getStringRoles()
    {
        return roles == null ? null :
                roles.stream().map(UserRole::name).collect(Collectors.toList());
    }
}
