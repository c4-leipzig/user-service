package org.c4.userservice.user.mapping;

/**
 * Mögliche Ausprägungen, die ein Attribut in einem UserMapping annehmen kann.
 * <br/>
 * Copyright: Copyright (c) 18.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public enum UserMappingValue
{
    /**
     * Das Attribut kann nur vom eigenen User und Admins gelesen werden.
     */
    PRIVATE,
    /**
     * Das Attribut kann von jedem User gelesen werden.
     */
    AUTHENTICATED,
    /**
     * Das Attribut kann von jedem gelesen werden.
     */
    PUBLIC;

    /**
     * Wandelt einen String in den entsprechenden Enum-Wert um. Die Methode ist Save, d.h. es wird erwartet,
     * dass nur wirklich gültige Werte eingespeist werden.
     */
    public static UserMappingValue ofSave(String value)
    {
        return valueOf(value);
    }
}
