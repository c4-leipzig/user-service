package org.c4.userservice.user.mapping.events;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.c4.microservice.framework.communication.persistence.AbstractEntity;
import org.c4.microservice.framework.domainevent.communication.UpdateDomainEvent;
import org.c4.userservice.domainevent.DomainEventHandlingContext;
import org.c4.userservice.user.data.UserEntity;
import org.c4.userservice.user.mapping.data.UserMappingChangeDto;

import java.util.Optional;

@Getter
@Setter
@Log4j2
@NoArgsConstructor
public class UserMappingUpdatedEvent extends UpdateDomainEvent
{
    private UserMappingChangeDto aggregate;

    public UserMappingUpdatedEvent(String aggregateId, String createdBy, Long version,
            UserMappingChangeDto aggregate)
    {
        super(aggregateId, createdBy);
        this.version = version;
        this.aggregate = aggregate;
    }

    @Override
    public void apply(DomainEventHandlingContext handlingContext, AbstractEntity persistedAggregate)
    {
        UserEntity userEntity = (UserEntity) persistedAggregate;
        userEntity.setVersion(version);
        userEntity.setLastModifiedBy(createdBy);
        userEntity.setLastModifiedAt(createdAt);
        userEntity.setUserPersistenceDto(aggregate.apply(userEntity.getUserPersistenceDto()));
        handlingContext.handleUserEvent(userEntity);
        log.debug("UserMappingUpdatedEvent applied");
    }

    @Override
    protected Optional<? extends AbstractEntity> getAggregate(
            DomainEventHandlingContext handlingContext)
    {
        return handlingContext.getUserAggregate(aggregateId);
    }
}
