package org.c4.userservice.user.mapping.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.c4.microservice.framework.communication.delta.Delta;
import org.c4.userservice.user.data.UserPersistenceDto;
import org.c4.userservice.user.mapping.UserMappingValue;

import java.util.Optional;

/**
 * Delta-Objekt, das eine Änderung der UserMappings repräsentiert.
 * <br/>
 * Copyright: Copyright (c) 10.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@NoArgsConstructor
public class UserMappingChangeDto implements Delta<UserPersistenceDto>
{
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private UserMappingValue email;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private UserMappingValue userType;

    @Override
    public UserPersistenceDto apply(UserPersistenceDto baseObject)
    {
        Optional.ofNullable(email).ifPresent(baseObject::setEmail);
        Optional.ofNullable(userType).ifPresent(baseObject::setUserType);

        return baseObject;
    }

    @Override
    public boolean isEmpty()
    {
        return email == null && userType == null;
    }
}
