package org.c4.userservice.user.mapping.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.c4.microservice.framework.validation.enums.EnumValue;
import org.c4.userservice.ErrorKey;
import org.c4.userservice.user.mapping.UserMappingValue;

@Getter
@Setter
@NoArgsConstructor
public class UserMappingRestDto
{
    @EnumValue(enumClazz = UserMappingValue.class, message = ErrorKey.EMAIL_USER_MAPPING_INVALID)
    private String email;

    @EnumValue(enumClazz = UserMappingValue.class, message = ErrorKey.USER_TYPE_USER_MAPPING_INVALID)
    private String userType;
}
