package org.c4.userservice.user.mapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.c4.microservice.framework.communication.ResponseEntityBuilder;
import org.c4.microservice.framework.communication.VoidProcessingResult;
import org.c4.userservice.user.UserService;
import org.c4.userservice.user.data.UserIdDto;
import org.c4.userservice.user.mapping.data.UserMappingRestDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

/**
 * Controller zum Zugriff auf UserMapping-Resourcen.
 * <br/>
 * Copyright: Copyright (c) 20.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@RestController
@Tag(name = "Users")
public class UserMappingController
{
    private final UserService userService;

    public UserMappingController(UserService userService)
    {
        this.userService = userService;
    }

    @PutMapping("/users/self/mapping")
    @PreAuthorize("isAuthenticated()")
    @ApiResponse(responseCode = "200", description = "User mapping updated successfully.")
    @ApiResponse(responseCode = "400", description = "User mapping could not be updated.")
    @Operation(summary = "Update the mapping for the own users attributes.")
    public ResponseEntity<VoidProcessingResult> updateOwnUserMapping(
            @RequestBody @Valid UserMappingRestDto userMappingRestDto, Principal principal)
    {
        return ResponseEntityBuilder.build(
                userService.updateUserMapping(userMappingRestDto, UserIdDto.of(principal.getName()),
                        UserIdDto.of(principal.getName())), OK, BAD_REQUEST);
    }

    @PutMapping("/users/{id}/mapping")
    @PreAuthorize("hasRole('USER_ADMIN')")
    @ApiResponse(responseCode = "200", description = "User mapping updated successfully.")
    @ApiResponse(responseCode = "400", description = "User mapping could not be updated.")
    @Operation(summary = "Update the mapping for a users attributes.")
    public ResponseEntity<VoidProcessingResult> updateOtherUserMapping(
            @RequestBody @Valid UserMappingRestDto userMappingRestDto, @Valid UserIdDto userId,
            Principal principal)
    {
        return ResponseEntityBuilder.build(userService.updateUserMapping(userMappingRestDto, userId,
                UserIdDto.of(principal.getName())), OK, BAD_REQUEST);
    }
}
