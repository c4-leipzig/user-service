package org.c4.userservice.user;

import org.c4.microservice.framework.domainevent.messaging.EventPublisher;
import org.c4.userservice.user.data.UserEntity;
import org.c4.userservice.user.data.UserIdDto;
import org.c4.userservice.user.data.UserPersistenceDto;
import org.c4.userservice.user.data.UserStatusChangeDto;
import org.c4.userservice.user.events.UserCreatedEvent;
import org.c4.userservice.user.events.UserStatusUpdatedEvent;
import org.c4.userservice.user.mapping.data.UserMappingChangeDto;
import org.c4.userservice.user.mapping.events.UserMappingUpdatedEvent;

import java.security.Principal;

/**
 * Service zur Erstellung von UserEvents.
 * <br/>
 * Copyright: Copyright (c) 14.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class UserEventService
{
    private final EventPublisher eventPublisher;

    public UserEventService(EventPublisher eventPublisher)
    {
        this.eventPublisher = eventPublisher;
    }

    public void dispatchUserCreatedEvent(UserIdDto userId, Principal principal)
    {
        UserPersistenceDto userPersistenceDto = UserPersistenceDto.initial(userId.getId());

        UserCreatedEvent event = new UserCreatedEvent(principal);
        event.setAggregate(userPersistenceDto);

        eventPublisher.send(event);
    }

    public void dispatchUserMappingUpdatedEvent(UserEntity userEntity,
            UserMappingChangeDto changeDto, UserIdDto createdBy)
    {
        UserMappingUpdatedEvent event = new UserMappingUpdatedEvent(userEntity.getAggregateId(),
                createdBy.getId(), userEntity.getVersion(), changeDto);

        eventPublisher.send(event);
    }

    public void dispatchUserStatusUpdatedEvent(UserEntity userEntity, UserStatusChangeDto changeDto,
            UserIdDto createdBy)
    {
        UserStatusUpdatedEvent event = new UserStatusUpdatedEvent(userEntity.getAggregateId(),
                createdBy.getId(), userEntity.getVersion(), changeDto);

        eventPublisher.send(event);
    }
}
