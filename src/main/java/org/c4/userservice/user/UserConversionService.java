package org.c4.userservice.user;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.c4.userservice.user.data.*;
import org.c4.userservice.user.mapping.UserMappingValue;
import org.c4.userservice.user.mapping.data.UserMappingChangeDto;
import org.c4.userservice.user.mapping.data.UserMappingRestDto;

/**
 * Service zur Umwandlung von UserRest-/-DataDtos.
 * <br/>
 * Copyright: Copyright (c) 14.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserConversionService
{
    public static UserRestDto transformToFullUser(UserDataDto userDataDto)
    {
        UserRestDto userRestDto = new UserRestDto();
        userRestDto.setUserId(userDataDto.getUserId());
        userRestDto.setEmail(userDataDto.getEmail());
        userRestDto.setUsername(userDataDto.getUsername());
        userRestDto.setUserType(userDataDto.getUserType().name());

        return userRestDto;
    }

    public static UserPersistenceDto transformToDataDto(UserMappingRestDto restDto, String userId)
    {
        UserPersistenceDto dataDto = new UserPersistenceDto();
        dataDto.setUserId(userId);
        dataDto.setEmail(UserMappingValue.ofSave(restDto.getEmail()));
        dataDto.setUserType(UserMappingValue.ofSave(restDto.getUserType()));

        return dataDto;
    }

    public static UserMappingChangeDto createDiff(UserEntity userEntity, UserMappingRestDto restDto)
    {
        UserMappingChangeDto changeDto = new UserMappingChangeDto();

        UserMappingValue emailMapping = UserMappingValue.ofSave(restDto.getEmail());
        if (!emailMapping.equals(userEntity.getUserPersistenceDto().getEmail()))
        {
            changeDto.setEmail(emailMapping);
        }

        UserMappingValue userTypeMapping = UserMappingValue.ofSave(restDto.getUserType());
        if (!userTypeMapping.equals(userEntity.getUserPersistenceDto().getUserType()))
        {
            changeDto.setUserType(userTypeMapping);
        }

        return changeDto;
    }

    public static UserStatusChangeDto createDiff(UserEntity userEntity, UserStatusRestDto restDto)
    {
        UserStatusChangeDto changeDto = new UserStatusChangeDto();

        UserStatus userStatus = UserStatus.ofSave(restDto.getUserStatus());
        if (!userStatus.equals(userEntity.getUserPersistenceDto().getUserStatus()))
        {
            changeDto.setUserStatus(userStatus);
        }

        return changeDto;
    }
}
