package org.c4.userservice.user;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.c4.microservice.framework.communication.ProcessingResult;
import org.c4.microservice.framework.communication.ResponseEntityBuilder;
import org.c4.microservice.framework.communication.VoidProcessingResult;
import org.c4.userservice.user.data.*;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

import static org.springframework.http.HttpStatus.*;

/**
 * Controller zum Zugriff auf User-Funktionen.
 * <br/>
 * Copyright: Copyright (c) 12.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@RestController
@Tag(name = "Users", description = "Endpoints for handling users.")
public class UserController
{
    private final UserService userService;

    public UserController(UserService userService)
    {
        this.userService = userService;
    }

    /**
     * Legt einen neuen User an.
     */
    @PostMapping("/users")
    @PreAuthorize("hasRole('USER_ADMIN')")
    @ApiResponse(responseCode = "201", description = "User created successfully.")
    @ApiResponse(responseCode = "400", description = "User could not be created.")
    @Operation(summary = "Create a new user.")
    public ResponseEntity<ProcessingResult<UserIdDto>> createUser(
            @RequestBody @Valid UserCreateRestDto user, Principal principal)
    {
        ProcessingResult<UserIdDto> createdUserId = userService.createUser(user, principal);

        return ResponseEntityBuilder.build(createdUserId, CREATED, BAD_REQUEST);
    }

    @PutMapping("/users/{id}")
    @PreAuthorize("hasRole('USER_ADMIN')")
    @ApiResponse(responseCode = "204", description = "User updated successfully.")
    @ApiResponse(responseCode = "400", description = "User could not be updated.")
    @Operation(summary = "Update manageable properties of a user.")
    public ResponseEntity<VoidProcessingResult> updateUserManaged(@Valid UserIdDto userId,
            @RequestBody @Valid UserManagedUpdateRestDto user)
    {
        return ResponseEntityBuilder.build(userService.updateUserManaged(userId, user), NO_CONTENT,
                BAD_REQUEST, NOT_FOUND);
    }

    /**
     * Gibt einen User anhand seiner Id zurück.
     */
    @GetMapping("/users/{id}")
    @PreAuthorize("hasRole('USER_ADMIN')")
    @ApiResponse(responseCode = "200", description = "User retrieved successfully.")
    @ApiResponse(responseCode = "404", description = "User could not be retrieved.")
    @Operation(summary = "Get a user by its id.")
    public ResponseEntity<ProcessingResult<UserRestDto>> getUserById(@Valid UserIdDto userId)
    {
        ProcessingResult<UserRestDto> result = new ProcessingResult<>();
        userService.getUserById(userId.getId()).ifPresent(result::setResult);

        return ResponseEntityBuilder.build(result, OK, NOT_FOUND);
    }

    @PutMapping("/users/{id}/status")
    @PreAuthorize("hasRole('USER_ADMIN')")
    @ApiResponse(responseCode = "204", description = "User status updated successfully.")
    @ApiResponse(responseCode = "400", description = "User status could not be updated.")
    @ApiResponse(responseCode = "404", description = "User could not be found.")
    @Operation(summary = "Update the status of the given user.")
    public ResponseEntity<VoidProcessingResult> updateUserStatus(@Valid UserIdDto userId,
            @RequestBody @Valid UserStatusRestDto restDto, Principal principal)
    {
        return ResponseEntityBuilder.build(
                userService.updateUserStatus(userId, restDto, UserIdDto.of(principal.getName())),
                NO_CONTENT, BAD_REQUEST, NOT_FOUND);
    }

    @GetMapping("/users/{id}/type")
    @PreAuthorize("hasRole('backend')")
    @ApiResponse(responseCode = "200", description = "Type for user successfully retrieved.")
    @ApiResponse(responseCode = "404", description = "User could not be found.")
    @Operation(summary = "Get the type of the given user.")
    public ResponseEntity<ProcessingResult<UserType>> getTypeForUser(@Valid UserIdDto userId)
    {
        return ResponseEntityBuilder.build(userService.getTypeForUser(userId.getId()), OK,
                BAD_REQUEST, NOT_FOUND);
    }
}
