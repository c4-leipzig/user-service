package org.c4.userservice.user;

import org.c4.microservice.framework.communication.ExtendedProcessingResult;
import org.c4.microservice.framework.communication.ProcessingResult;
import org.c4.microservice.framework.communication.VoidProcessingResult;
import org.c4.userservice.ErrorKey;
import org.c4.userservice.identityprovider.IdentityProviderRepository;
import org.c4.userservice.identityprovider.UserProcessingState;
import org.c4.userservice.user.data.*;
import org.c4.userservice.user.mapping.data.UserMappingChangeDto;
import org.c4.userservice.user.mapping.data.UserMappingRestDto;
import org.c4.userservice.user.persistence.UserRepository;
import org.c4.userservice.user.role.UserRoleConversionService;
import org.c4.userservice.user.role.data.UserRoleRestDto;

import java.security.Principal;
import java.util.Optional;

/**
 * Service für die Verwaltung von Usern.
 * <br/>
 * Copyright: Copyright (c) 12.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class UserService
{
    private final IdentityProviderRepository identityProviderRepository;
    private final UserEventService           userEventService;
    private final UserRepository             userRepository;

    public UserService(IdentityProviderRepository identityProviderRepository,
            UserEventService userEventService, UserRepository userRepository)
    {
        this.identityProviderRepository = identityProviderRepository;
        this.userEventService = userEventService;
        this.userRepository = userRepository;
    }

    public ProcessingResult<UserIdDto> createUser(UserCreateRestDto userCreateRestDto,
            Principal principal)
    {
        ExtendedProcessingResult<UserIdDto, UserProcessingState> createResult = identityProviderRepository
                .createUser(userCreateRestDto);

        if (createResult.hasProcessingState(UserProcessingState.CREATED))
        {
            userEventService.dispatchUserCreatedEvent(createResult.getResult(), principal);
        }

        return createResult;
    }

    public VoidProcessingResult updateUserManaged(UserIdDto userId,
            UserManagedUpdateRestDto userManagedUpdateRestDto)
    {
        return identityProviderRepository.updateUserManaged(userId, userManagedUpdateRestDto);
    }

    public Optional<UserRestDto> getUserById(String id)
    {
        return identityProviderRepository.getUserById(id)
                .map(UserConversionService::transformToFullUser);
    }

    public ProcessingResult<UserType> getTypeForUser(String id)
    {
        return ProcessingResult.ofOptional(identityProviderRepository.getUserById(id),
                ErrorKey.USER_NOT_FOUND).map(UserDataDto::getUserType);
    }

    public VoidProcessingResult updateUserMapping(UserMappingRestDto restDto, UserIdDto userId,
            UserIdDto createdBy)
    {
        VoidProcessingResult result = new VoidProcessingResult();

        userRepository.findByUserId(userId.getId()).ifPresentOrElse(userEntity -> {
            UserMappingChangeDto changeDto = UserConversionService.createDiff(userEntity, restDto);
            if (changeDto.isEmpty())
            {
                result.addError(ErrorKey.UPDATE_IDENTICAL);
            }
            else
            {
                userEventService.dispatchUserMappingUpdatedEvent(userEntity, changeDto, createdBy);
            }
        }, () -> result.addError(ErrorKey.USER_NOT_FOUND));

        return result;
    }

    public ProcessingResult<UserRoleRestDto> getUserRoles(UserIdDto userId)
    {
        return identityProviderRepository.getUserRoles(userId.getId())
                .map(UserRoleConversionService::transformToRestDto);
    }

    public VoidProcessingResult addRolesToUser(UserIdDto userId, UserRoleRestDto restDto)
    {
        return identityProviderRepository.addRoleToUser(
                UserRoleConversionService.transformToDataDto(restDto, userId));
    }

    public VoidProcessingResult removeRolesFromUser(UserIdDto userId, UserRoleRestDto restDto)
    {
        return identityProviderRepository.removeRoleFromUser(
                UserRoleConversionService.transformToDataDto(restDto, userId));
    }

    public VoidProcessingResult updateUserStatus(UserIdDto userId, UserStatusRestDto restDto,
            UserIdDto createdBy)
    {
        VoidProcessingResult result = new VoidProcessingResult();

        userRepository.findByUserId(userId.getId()).ifPresentOrElse(userEntity -> {
            UserStatusChangeDto changeDto = UserConversionService.createDiff(userEntity, restDto);
            if (changeDto.isEmpty())
            {
                result.addError(ErrorKey.UPDATE_IDENTICAL);
            }
            else
            {
                userEventService.dispatchUserStatusUpdatedEvent(userEntity, changeDto, createdBy);
            }
        }, () -> result.addError(ErrorKey.USER_NOT_FOUND));

        return result;
    }
}
