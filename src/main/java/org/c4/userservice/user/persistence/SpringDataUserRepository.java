package org.c4.userservice.user.persistence;

import org.c4.userservice.user.data.UserEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

/**
 * SpringData-Repository für die Persistierung von {@link UserEntity}-Objekten.
 * <br/>
 * Copyright: Copyright (c) 10.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface SpringDataUserRepository extends MongoRepository<UserEntity, String>
{
    Optional<UserEntity> findByUserPersistenceDto_userId(String userId);
}
