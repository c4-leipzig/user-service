package org.c4.userservice.user.persistence;

import org.c4.userservice.user.data.UserEntity;

import java.util.Optional;

/**
 * Repository zur Verwaltung von UserMappings.
 * <br/>
 * Copyright: Copyright (c) 12.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface UserRepository
{
    void save(UserEntity userEntity);

    Optional<UserEntity> findByAggregateId(String aggregateId);

    Optional<UserEntity> findByUserId(String userId);
}
