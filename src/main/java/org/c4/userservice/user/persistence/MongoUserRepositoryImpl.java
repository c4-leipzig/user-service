package org.c4.userservice.user.persistence;

import org.c4.userservice.user.data.UserEntity;

import java.util.Optional;

/**
 * Standardimplementation des {@link UserRepository}s.
 * <br/>
 * Copyright: Copyright (c) 12.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class MongoUserRepositoryImpl implements UserRepository
{
    private final SpringDataUserRepository springDataUserRepository;

    public MongoUserRepositoryImpl(SpringDataUserRepository springDataUserRepository)
    {
        this.springDataUserRepository = springDataUserRepository;
    }

    @Override
    public void save(UserEntity userEntity)
    {
        springDataUserRepository.save(userEntity);
    }

    @Override
    public Optional<UserEntity> findByAggregateId(String aggregateId)
    {
        return springDataUserRepository.findById(aggregateId);
    }

    @Override
    public Optional<UserEntity> findByUserId(String userId)
    {
        return springDataUserRepository.findByUserPersistenceDto_userId(userId);
    }
}
