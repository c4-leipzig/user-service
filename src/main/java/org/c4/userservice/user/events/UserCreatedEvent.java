package org.c4.userservice.user.events;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.c4.microservice.framework.domainevent.communication.CreateDomainEvent;
import org.c4.userservice.domainevent.DomainEventHandlingContext;
import org.c4.userservice.user.data.UserEntity;
import org.c4.userservice.user.data.UserPersistenceDto;

import java.security.Principal;

/**
 * Event zur initialen Erstellung eines UserMappings.
 * <br/>
 * Copyright: Copyright (c) 15.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@Log4j2
@NoArgsConstructor
public class UserCreatedEvent extends CreateDomainEvent
{
    private UserPersistenceDto aggregate;

    public UserCreatedEvent(Principal createdBy)
    {
        super(createdBy);
    }

    @Override
    protected void apply(DomainEventHandlingContext handlingContext)
    {
        UserEntity userEntity = getInitialAggregate();
        handlingContext.handleUserEvent(userEntity);
        log.debug("UserCreatedEvent applied");
    }

    @Override
    protected boolean aggregatePresent(DomainEventHandlingContext handlingContext)
    {
        return handlingContext.getUserAggregate(aggregateId).isPresent();
    }

    private UserEntity getInitialAggregate()
    {
        UserEntity userEntity = UserEntity.of(this);
        userEntity.setUserPersistenceDto(aggregate);

        return userEntity;
    }
}
