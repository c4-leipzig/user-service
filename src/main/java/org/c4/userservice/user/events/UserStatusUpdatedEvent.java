package org.c4.userservice.user.events;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.c4.microservice.framework.communication.persistence.AbstractEntity;
import org.c4.microservice.framework.domainevent.communication.UpdateDomainEvent;
import org.c4.userservice.domainevent.DomainEventHandlingContext;
import org.c4.userservice.user.data.UserEntity;
import org.c4.userservice.user.data.UserStatusChangeDto;

import java.util.Optional;

/**
 * Event zur Persistierung einer User-Statusänderung.
 * <br/>
 * Copyright: Copyright (c) 10.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@Log4j2
@NoArgsConstructor
public class UserStatusUpdatedEvent extends UpdateDomainEvent
{
    private UserStatusChangeDto changeDto;

    public UserStatusUpdatedEvent(String aggregateId, String createdBy, long version,
            UserStatusChangeDto changeDto)
    {
        super(aggregateId, createdBy, version);
        this.changeDto = changeDto;
    }

    @Override
    protected void apply(DomainEventHandlingContext handlingContext, AbstractEntity aggregate)
    {
        UserEntity userEntity = (UserEntity) aggregate;
        userEntity.setVersion(version);
        userEntity.setLastModifiedBy(createdBy);
        userEntity.setLastModifiedAt(createdAt);
        userEntity.setUserPersistenceDto(changeDto.apply(userEntity.getUserPersistenceDto()));

        handlingContext.handleUserEvent(userEntity);
        log.debug("UserStatusUpdatedEvent handled successfully.");
    }

    @Override
    protected Optional<? extends AbstractEntity> getAggregate(
            DomainEventHandlingContext handlingContext)
    {
        return handlingContext.getUserAggregate(aggregateId);
    }
}
