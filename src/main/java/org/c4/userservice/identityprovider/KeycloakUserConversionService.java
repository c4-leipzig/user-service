package org.c4.userservice.identityprovider;

import lombok.AllArgsConstructor;
import org.c4.userservice.user.data.UserCreateRestDto;
import org.c4.userservice.user.data.UserDataDto;
import org.c4.userservice.user.data.UserManagedUpdateRestDto;
import org.c4.userservice.user.data.UserType;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Converter zur Umwandlung von {@link UserDataDto}s.
 * Klasse ist package-private, da sie Keycloak-spezifisch ist.
 * <br/>
 * Copyright: Copyright (c) 14.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@AllArgsConstructor
public class KeycloakUserConversionService
{
    private final String USER_TYPE_ATTRIBUTE_KEY;

    public UserDataDto transformToUserDataDto(UserRepresentation userRepresentation)
    {
        UserDataDto user = new UserDataDto();
        user.setUserId(userRepresentation.getId());
        user.setUsername(userRepresentation.getUsername());
        user.setEmail(userRepresentation.getEmail());
        Optional.ofNullable(userRepresentation.getAttributes().get(USER_TYPE_ATTRIBUTE_KEY))
                .map(userTypes -> userTypes.get(0))
                .ifPresent(userType -> user.setUserType(UserType.ofSave(userType)));

        return user;
    }

    public UserRepresentation transformToUserRepresentation(UserCreateRestDto restDto)
    {
        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setEnabled(true);
        userRepresentation.setEmail(restDto.getEmail());
        userRepresentation.setUsername(restDto.getUsername());

        Map<String, List<String>> attributeMap = KeycloakAttributeBuilder.empty()
                .addKey(USER_TYPE_ATTRIBUTE_KEY, restDto.getUserType())
                .build();

        userRepresentation.setAttributes(attributeMap);

        return userRepresentation;
    }

    public void updateUserRepresentation(UserRepresentation user, UserManagedUpdateRestDto restDto)
    {
        Map<String, List<String>> attributeMap = KeycloakAttributeBuilder.of(user)
                .addKey(USER_TYPE_ATTRIBUTE_KEY, restDto.getUserType())
                .build();

        user.setAttributes(attributeMap);
    }
}
