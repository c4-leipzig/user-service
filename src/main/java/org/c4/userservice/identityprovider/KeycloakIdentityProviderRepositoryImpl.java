package org.c4.userservice.identityprovider;

import lombok.extern.log4j.Log4j2;
import org.c4.microservice.framework.communication.ExtendedProcessingResult;
import org.c4.microservice.framework.communication.ProcessingResult;
import org.c4.microservice.framework.communication.VoidProcessingResult;
import org.c4.userservice.ErrorKey;
import org.c4.userservice.user.data.UserCreateRestDto;
import org.c4.userservice.user.data.UserDataDto;
import org.c4.userservice.user.data.UserIdDto;
import org.c4.userservice.user.data.UserManagedUpdateRestDto;
import org.c4.userservice.user.role.data.UserRole;
import org.c4.userservice.user.role.data.UserRoleDataDto;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.c4.userservice.identityprovider.UserProcessingState.*;

/**
 * Keycloak-Implementation des {@link IdentityProviderRepository}s.
 * <br/>
 * Copyright: Copyright (c) 12.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
public class KeycloakIdentityProviderRepositoryImpl implements IdentityProviderRepository
{
    private final RealmResource realmResource;

    private final List<String> EXCLUDED_ROLES = Arrays.asList("uma_authorization",
            "offline_access");

    private final KeycloakUserConversionService keycloakUserConversionService;

    public KeycloakIdentityProviderRepositoryImpl(RealmResource realmResource,
            KeycloakUserConversionService keycloakUserConversionService)
    {
        this.realmResource = realmResource;
        this.keycloakUserConversionService = keycloakUserConversionService;
    }

    @Override
    public ExtendedProcessingResult<UserIdDto, UserProcessingState> createUser(
            UserCreateRestDto userCreateRestDto)
    {
        UserRepresentation user = keycloakUserConversionService.transformToUserRepresentation(
                userCreateRestDto);

        ExtendedProcessingResult<UserIdDto, UserProcessingState> result = new ExtendedProcessingResult<>();
        try (Response response = realmResource.users().create(user))
        {
            int responseStatus = response.getStatusInfo().getStatusCode();
            switch (responseStatus)
            {
            case 201:
                result.setProcessingState(CREATED);
                result.setResult(extractUserIdFromResponse(response));
                break;
            case 409:
                result.setProcessingState(CONFLICT);
                result.addError(ErrorKey.USERNAME_OR_EMAIL_TAKEN);
                break;
            default:
                result.setProcessingState(ERROR);
                result.addError(ErrorKey.UNDEFINED_KEYCLOAK_ERROR);
            }
        }
        catch (Exception e)
        {
            log.warn("Error creating user", e);
            result.addError(ErrorKey.UNDEFINED_KEYCLOAK_ERROR);
            result.setProcessingState(ERROR);
        }

        return result;
    }

    @Override
    public VoidProcessingResult updateUserManaged(UserIdDto userId,
            UserManagedUpdateRestDto updateRestDto)
    {
        VoidProcessingResult result = new VoidProcessingResult();

        getUserFromKeycloak(userId.getId()).ifPresentOrElse(userResource -> {
            UserRepresentation userRepresentation = userResource.toRepresentation();
            keycloakUserConversionService.updateUserRepresentation(userRepresentation,
                    updateRestDto);

            try
            {
                userResource.update(userRepresentation);
                log.debug("User '{}' updated successfully", userId::getId);
            }
            catch (Exception e)
            {
                result.addError(ErrorKey.UNDEFINED_KEYCLOAK_ERROR);
                log.warn("Error updating user '{}'", userId::getId);
                log.catching(e);
            }
        }, () -> {
            result.addError(ErrorKey.USER_NOT_FOUND);
            log.info("User with id '{}' was not found.", userId::getId);
        });

        return result;
    }

    private UserIdDto extractUserIdFromResponse(Response response)
    {
        String path = response.getLocation().getPath();
        String[] pathSegments = path.split("/");
        return new UserIdDto(pathSegments[pathSegments.length - 1]);
    }

    @Override
    public Optional<UserDataDto> getUserById(String id)
    {
        return getUserFromKeycloak(id).map(UserResource::toRepresentation)
                .map(keycloakUserConversionService::transformToUserDataDto);
    }

    @Override
    public ProcessingResult<UserRoleDataDto> getUserRoles(String userId)
    {
        ProcessingResult<UserRoleDataDto> result = new ProcessingResult<>();
        getRolesForUser(userId).ifPresentOrElse(result::setResult,
                () -> result.addError(ErrorKey.USER_NOT_FOUND));

        return result;
    }

    @Override
    public VoidProcessingResult addRoleToUser(UserRoleDataDto userRoleDataDto)
    {
        Optional<UserResource> optUserResource = getUserFromKeycloak(userRoleDataDto.getUserId());
        VoidProcessingResult result = new VoidProcessingResult();

        optUserResource.ifPresentOrElse(
                userResource -> getRoleRepresentationsFor(userRoleDataDto.getRoles()).ifValidOrElse(
                        roleList -> {
                            try
                            {
                                userResource.roles().realmLevel().add(roleList);
                            }
                            catch (Exception e)
                            {
                                log.warn("Error adding roles '{}' to user",
                                        userRoleDataDto::getStringRoles);
                                log.catching(e);
                                result.addError(ErrorKey.UNDEFINED_KEYCLOAK_ERROR);
                            }
                        }, () -> result.addError(ErrorKey.ROLE_NOT_FOUND)),
                () -> result.addError(ErrorKey.USER_NOT_FOUND));

        return result;
    }

    @Override
    public VoidProcessingResult removeRoleFromUser(UserRoleDataDto userRoleDataDto)
    {
        Optional<UserResource> optUserResource = getUserFromKeycloak(userRoleDataDto.getUserId());
        VoidProcessingResult result = new VoidProcessingResult();

        optUserResource.ifPresentOrElse(
                userResource -> getRoleRepresentationsFor(userRoleDataDto.getRoles()).ifValidOrElse(
                        roleList -> {
                            try
                            {
                                userResource.roles().realmLevel().remove(roleList);
                            }
                            catch (Exception e)
                            {
                                log.warn("Error removing roles '{}' from user",
                                        userRoleDataDto::getStringRoles);
                                log.catching(e);
                                result.addError(ErrorKey.UNDEFINED_KEYCLOAK_ERROR);
                            }
                        }, () -> result.addError(ErrorKey.ROLE_NOT_FOUND)),
                () -> result.addError(ErrorKey.USER_NOT_FOUND));

        return result;
    }

    private ProcessingResult<List<RoleRepresentation>> getRoleRepresentationsFor(
            List<UserRole> roles)
    {
        ProcessingResult<List<RoleRepresentation>> result = getRoleRepresentations();
        List<String> roleNames = roles.stream().map(UserRole::name).collect(Collectors.toList());

        return result.map(allRoles -> allRoles.stream()
                .filter(roleRepresentation -> roleNames.contains(roleRepresentation.getName()))
                .collect(Collectors.toList()));
    }

    private ProcessingResult<List<RoleRepresentation>> getRoleRepresentations()
    {
        ProcessingResult<List<RoleRepresentation>> result = new ProcessingResult<>();

        try
        {
            List<RoleRepresentation> roles = realmResource.roles().list();
            if (roles != null && !roles.isEmpty())
            {
                result.setResult(roles);
            }
            else
            {
                result.addError(ErrorKey.ROLE_NOT_FOUND);
            }
        }
        catch (Exception e)
        {
            log.warn("Could not retrieve Roles from Keycloak", e);
            result.addError(ErrorKey.UNDEFINED_KEYCLOAK_ERROR);
        }

        return result;
    }

    private Optional<UserRoleDataDto> getRolesForUser(String userId)
    {
        Optional<UserRoleDataDto> result = Optional.empty();

        try
        {
            UserRoleDataDto dataDto = new UserRoleDataDto();
            dataDto.setUserId(userId);

            List<RoleRepresentation> roleRepresentations = realmResource.users()
                    .get(userId)
                    .roles()
                    .realmLevel()
                    .listAll();

            if (roleRepresentations != null)
            {
                dataDto.setRoles(roleRepresentations.stream()
                        .map(RoleRepresentation::getName)
                        .filter(role -> !EXCLUDED_ROLES.contains(role))
                        .map(UserRole::ofSave)
                        .collect(Collectors.toList()));
            }

            result = Optional.of(dataDto);
        }
        catch (Exception e)
        {
            log.warn("Roles for user with id {} could not be retrieved", userId);
        }

        return result;
    }

    private Optional<UserResource> getUserFromKeycloak(String userId)
    {
        Optional<UserResource> result;
        try
        {
            result = Optional.ofNullable(realmResource.users().get(userId));
            result.ifPresent(UserResource::toRepresentation);
        }
        catch (Exception e)
        {
            result = Optional.empty();
            log.warn("User with id {} could not be retrieved.", userId);
        }

        return result;
    }
}
