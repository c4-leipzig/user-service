package org.c4.userservice.identityprovider;

/**
 * Ausprägungen, die das Hinzufügen eines Users haben kann.
 * <br/>
 * Copyright: Copyright (c) 16.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public enum UserProcessingState
{
    CREATED,
    CONFLICT,
    ERROR
}
