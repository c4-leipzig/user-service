package org.c4.userservice.identityprovider;

import org.c4.microservice.framework.communication.ExtendedProcessingResult;
import org.c4.microservice.framework.communication.ProcessingResult;
import org.c4.microservice.framework.communication.VoidProcessingResult;
import org.c4.userservice.user.data.UserCreateRestDto;
import org.c4.userservice.user.data.UserDataDto;
import org.c4.userservice.user.data.UserIdDto;
import org.c4.userservice.user.data.UserManagedUpdateRestDto;
import org.c4.userservice.user.role.data.UserRoleDataDto;

import java.util.Optional;

public interface IdentityProviderRepository
{
    /**
     * Legt einen neuen User an.
     */
    ExtendedProcessingResult<UserIdDto, UserProcessingState> createUser(
            UserCreateRestDto userCreateRestDto);

    /**
     * Methode zum Update eines Users durch einen Administrator.
     */
    VoidProcessingResult updateUserManaged(UserIdDto userId,
            UserManagedUpdateRestDto updateRestDto);

    /**
     * Liefert einen User anhand seiner Id zurück.
     */
    Optional<UserDataDto> getUserById(String id);

    /**
     * Liefert die Rollen eines Users zurück.
     */
    ProcessingResult<UserRoleDataDto> getUserRoles(String userId);

    /**
     * Fügt Rollen zu einem User hinzu.
     */
    VoidProcessingResult addRoleToUser(UserRoleDataDto userRoleDataDto);

    /**
     * Entfernt Rollen eines Users.
     */
    VoidProcessingResult removeRoleFromUser(UserRoleDataDto userRoleDataDto);
}
