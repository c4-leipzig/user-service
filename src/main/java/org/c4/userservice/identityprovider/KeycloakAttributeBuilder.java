package org.c4.userservice.identityprovider;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.*;

/**
 * Wrapper für eine Map<String, List<String>>. Diese wird intern von Keycloak zur Verwaltung
 * von Userattributen verwendet.
 * <br/>
 * Copyright: Copyright (c) 10.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class KeycloakAttributeBuilder
{
    private final Map<String, List<String>> attributeMap;

    /**
     * Liefert eine leere Map zurück.
     */
    public static KeycloakAttributeBuilder empty()
    {
        return new KeycloakAttributeBuilder(new HashMap<>());
    }

    /**
     * Erzeugt eine Map aus der übergebenen Map.
     */
    public static KeycloakAttributeBuilder of(Map<String, List<String>> attributeMap)
    {
        return new KeycloakAttributeBuilder(attributeMap);
    }

    /**
     * Extrahiert die Map aus einer UserResource.
     */
    public static KeycloakAttributeBuilder of(UserResource userResource)
    {
        Map<String, List<String>> attributeMap = userResource.toRepresentation().getAttributes();
        return new KeycloakAttributeBuilder(attributeMap == null ? new HashMap<>() : attributeMap);
    }

    /**
     * Extrahiert die Map aus einer UserRepresentation.
     */
    public static KeycloakAttributeBuilder of(UserRepresentation userRepresentation)
    {
        Map<String, List<String>> attributeMap = userRepresentation.getAttributes();
        return new KeycloakAttributeBuilder(attributeMap == null ? new HashMap<>() : attributeMap);
    }

    /**
     * Erzeugt aus einem Key-Value-Pair eine Map.
     */
    public static KeycloakAttributeBuilder of(String key, String value)
    {
        Map<String, List<String>> attributeMap = new HashMap<>();
        attributeMap.put(key, Collections.singletonList(value));

        return new KeycloakAttributeBuilder(attributeMap);
    }

    /**
     * Erzeug aus einem Key-ValueList-Par eine Map.
     */
    public static KeycloakAttributeBuilder of(String key, List<String> values)
    {
        Map<String, List<String>> attributeMap = new HashMap<>();
        attributeMap.put(key, values);

        return new KeycloakAttributeBuilder(attributeMap);
    }

    /**
     * Fügt einer Map ein Key-Value-Pair hinzu.
     */
    public KeycloakAttributeBuilder addKey(String key, String value)
    {
        attributeMap.put(key, Collections.singletonList(value));

        return this;
    }

    /**
     * Fügt einer Map ein Key-ValueList-Pair hinzu.
     */
    public KeycloakAttributeBuilder addKey(String key, List<String> values)
    {
        attributeMap.put(key, values);

        return this;
    }

    /**
     * Fügt einer Liste aus der Map einen Wert hinzu.
     */
    public KeycloakAttributeBuilder addToKey(String key, String value)
    {
        List<String> list = attributeMap.get(key);
        List<String> newList;
        if (list != null)
        {
            newList = new ArrayList<>(list);
            newList.add(value);
        }
        else
        {
            newList = Collections.singletonList(value);
        }
        attributeMap.put(key, newList);

        return this;
    }

    /**
     * Fügt einer Liste aus der Map Werte einer anderen Liste hinzu.
     */
    public KeycloakAttributeBuilder addToKey(String key, List<String> values)
    {
        List<String> list = attributeMap.get(key);
        List<String> newList;
        if (list != null)
        {
            newList = new ArrayList<>(list);
            newList.addAll(values);
        }
        else
        {
            newList = values;
        }
        attributeMap.put(key, newList);

        return this;
    }

    /**
     * Löscht einen Key aus der Map.
     */
    public KeycloakAttributeBuilder removeKey(String key)
    {
        attributeMap.remove(key);

        return this;
    }

    /**
     * Löscht einen Wert aus der Liste, die einem Key zugeordnet ist.
     */
    public KeycloakAttributeBuilder removeFromKey(String key, String value)
    {
        List<String> currentList = attributeMap.get(key);
        List<String> newList;

        if (currentList != null)
        {
            newList = new ArrayList<>(currentList);
            newList.remove(value);
        }
        else
        {
            newList = new ArrayList<>();
        }

        if (newList.isEmpty())
        {
            attributeMap.remove(key);
        }
        else
        {
            attributeMap.put(key, newList);
        }

        return this;
    }

    /**
     * Löscht eine Liste von Wert aus der Liste, die einem Key zugeordnet ist.
     */
    public KeycloakAttributeBuilder removeFromKey(String key, List<String> values)
    {
        List<String> currentList = attributeMap.get(key);
        List<String> newList;

        if (currentList != null)
        {
            newList = new ArrayList<>(currentList);
            newList.removeAll(values);
        }
        else
        {
            newList = new ArrayList<>();
        }

        if (newList.isEmpty())
        {
            attributeMap.remove(key);
        }
        else
        {
            attributeMap.put(key, newList);
        }

        return this;
    }

    public Map<String, List<String>> build()
    {
        return attributeMap;
    }
}
