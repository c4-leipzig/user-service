package org.c4.microservice.framework.communication.delta;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Interface, das angibt, dass ein Objekt die Differenz eines Objekts angibt.
 * Die implementierende Klasse muss eine Methode zur Verfügung stellen, mit der das Delta auf ein
 * Objekt angewandt werden kann.
 * <br/>
 * Copyright: Copyright (c) 10.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface Delta<T>
{
    /**
     * Wendet das Delta auf einem Objekt an.
     */
    T apply(T baseObject);

    @JsonIgnore
    boolean isEmpty();
}
