package org.c4.microservice.framework.communication;

import lombok.extern.log4j.Log4j2;

import java.util.List;

/**
 * Ein {@link ProcessingResult}, das kein Resultat liefert, ansonsten aber genauso gehandhabt wird.
 * <br/>
 * Copyright: Copyright (c) 10.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
public class VoidProcessingResult extends ProcessingResult<Void>
{
    /**
     * Erzeugt ein Result direkt aus einem Fehler.
     */
    public static VoidProcessingResult ofError(String error)
    {
        VoidProcessingResult result = new VoidProcessingResult();
        result.addError(error);

        return result;
    }

    /**
     * Erzeugt ein Result direkt aus einer Liste von Fehlern.
     */
    public static VoidProcessingResult ofErrors(List<String> errors)
    {
        VoidProcessingResult result = new VoidProcessingResult();
        result.addAllErrors(errors);

        return result;
    }

    @Override
    public boolean isValid()
    {
        return super.isErrorFree();
    }

    @Override
    public void setResult(Void result)
    {
        log.error("Cannot set result on VoidProcessingResult!");
    }
}
