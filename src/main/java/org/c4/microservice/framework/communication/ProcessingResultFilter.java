package org.c4.microservice.framework.communication;

import lombok.NoArgsConstructor;

/**
 * Ein Filter, mit dem Jackson überprüft, ob ein {@link ProcessingResult} ausgegeben werden soll.
 * <br/>
 * Copyright: Copyright (c) 14.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@NoArgsConstructor
public class ProcessingResultFilter
{
    public boolean equals(ProcessingResult processingResult)
    {
        return processingResult.getResult() == null && processingResult.getErrors().isEmpty();
    }
}
