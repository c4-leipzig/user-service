package org.c4.microservice.framework.communication;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Wrapper für das Verarbeiten eines Resultats und etwaiger Fehler.
 * <br/>
 * Copyright: Copyright (c) 12.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@JsonInclude(value = JsonInclude.Include.CUSTOM, valueFilter = ProcessingResultFilter.class)
public class ProcessingResult<T>
{
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T result;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<String> errors = new ArrayList<>();

    /**
     * Erzeugt ein ProcessingResult direkt aus einem Result.
     */
    public static <T> ProcessingResult<T> of(T result)
    {
        ProcessingResult<T> processingResult = new ProcessingResult<>();
        processingResult.setResult(result);

        return processingResult;
    }

    /**
     * Erzeugt ein ProcessingResult ausgehend von einem Optional. Ist das Optional leer, wird der
     * übergebene Errorcode an das Result gehangen.
     */
    public static <T> ProcessingResult<T> ofOptional(
            @SuppressWarnings("OptionalUsedAsFieldOrParameterType") Optional<T> result,
            String notFoundError)
    {
        ProcessingResult<T> processingResult = new ProcessingResult<>();
        result.ifPresentOrElse(processingResult::setResult,
                () -> processingResult.addError(notFoundError));

        return processingResult;
    }

    public boolean hasError(String error)
    {
        return errors != null && errors.contains(error);
    }

    public boolean hasAllErrors(List<String> errors)
    {
        return this.errors != null && this.errors.containsAll(errors);
    }

    public void addError(String error)
    {
        initErrors();
        errors.add(error);
    }

    public void addAllErrors(List<String> errors)
    {
        this.errors.addAll(errors);
    }

    @JsonIgnore
    public boolean isValid()
    {
        return result != null && (errors == null || errors.isEmpty());
    }

    @JsonIgnore
    public boolean isErrorFree()
    {
        return errors == null || errors.isEmpty();
    }

    @JsonIgnore
    public boolean isErroneous()
    {
        return errors != null && !errors.isEmpty();
    }

    /**
     * {@link Optional} nachempfunden.
     */
    public ProcessingResult<T> ifPresent(Function<T, ?> function)
    {
        if (result != null)
        {
            function.apply(result);
        }

        return this;
    }

    public void ifValidOrElse(Consumer<T> validFunc, Runnable invalidFunc)
    {
        if (isValid())
        {
            validFunc.accept(result);
        }
        else
        {
            invalidFunc.run();
        }
    }

    public void ifValidOrElse(Consumer<T> validFunc, Consumer<List<String>> invalidFunc)
    {
        if (isValid())
        {
            validFunc.accept(result);
        }
        else
        {
            invalidFunc.accept(errors);
        }
    }

    /**
     * {@link Optional} nachempfunden.
     */
    public T orElse(T returnValue)
    {
        return result == null ? returnValue : result;
    }

    /**
     * {@link Optional} nachempfunden.
     */
    public <R> ProcessingResult<R> map(Function<T, R> function)
    {
        ProcessingResult<R> newResult = new ProcessingResult<>();
        newResult.addAllErrors(errors);
        if (result != null)
        {
            newResult.setResult(function.apply(result));
        }

        return newResult;
    }

    private void initErrors()
    {
        if (errors == null)
        {
            errors = new ArrayList<>();
        }
    }
}
