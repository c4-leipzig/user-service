package org.c4.microservice.framework.communication;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Helper-Class, über die zwei Objekte miteinander verglichen werden können.
 * <br/>
 * Copyright: Copyright (c) 20.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EqualHelper
{
    public static <T> boolean equalsNullSafe(T firstObject, T secondObject)
    {
        if (firstObject == null)
        {
            return secondObject == null;
        }

        return firstObject.equals(secondObject);
    }
}
