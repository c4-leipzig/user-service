package org.c4.microservice.framework.communication;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.function.Function;

/**
 * Erweitert das {@link ProcessingResult} um einen ProcessingState.
 * <br/>
 * Copyright: Copyright (c) 17.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
public class ExtendedProcessingResult<T, S> extends ProcessingResult<T>
{
    @JsonIgnore
    private S processingState;

    public boolean hasProcessingState(S processingState)
    {
        return processingStateSet() && this.processingState.equals(processingState);
    }

    public boolean processingStateSet()
    {
        return processingState != null;
    }

    @Override
    public <U> ExtendedProcessingResult<U, S> map(Function<T, U> function)
    {
        ExtendedProcessingResult<U, S> newResult = new ExtendedProcessingResult<>();
        newResult.setErrors(this.getErrors());
        newResult.setProcessingState(processingState);
        newResult.setResult(this.getResult() == null ? null : function.apply(this.getResult()));

        return newResult;
    }
}
