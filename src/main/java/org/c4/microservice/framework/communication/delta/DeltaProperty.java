package org.c4.microservice.framework.communication.delta;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Property eines Deltas. Es wird der neue Wert angegeben, oder das Attribut auf null gesetzt, falls
 * das Flag gesetzt ist.
 * <br/>
 * Copyright: Copyright (c) 21.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@NoArgsConstructor
public class DeltaProperty<T>
{
    @JsonInclude(JsonInclude.Include.NON_NULL)
    T       changeValue;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    boolean shouldBeNulled;

    public static <T> DeltaProperty<T> of(T value)
    {
        DeltaProperty<T> result = new DeltaProperty<>();
        if (value == null)
        {
            result.setShouldBeNulled(true);
        }
        else
        {
            result.setChangeValue(value);
        }

        return result;
    }

    public T apply()
    {
        return shouldBeNulled ? null : changeValue;
    }
}
