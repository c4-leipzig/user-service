package org.c4.microservice.framework.communication;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.c4.userservice.ErrorKey;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Utility-Class zum Erzeugen einer {@link ResponseEntity} aus einem {@link ProcessingResult}.
 * <br/>
 * Copyright: Copyright (c) 18.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ResponseEntityBuilder
{
    /**
     * Abhängig von der Validität des ProcessinResults wird der entsprechende HttpStatus gesetzt.
     */
    public static <T> ResponseEntity<ProcessingResult<T>> build(
            ProcessingResult<T> processingResult, HttpStatus validStatus, HttpStatus invalidStatus)
    {
        return ResponseEntity.status(processingResult.isValid() ? validStatus : invalidStatus)
                .body(processingResult);
    }

    /**
     * Der invalidStatus wird zusätzlich in NotFound und Other unterteilt.
     */
    public static <T> ResponseEntity<ProcessingResult<T>> build(
            ProcessingResult<T> processingResult, HttpStatus validStatus, HttpStatus invalidStatus,
            HttpStatus notFoundStatus)
    {
        HttpStatus status = buildStatus(processingResult, validStatus, invalidStatus,
                notFoundStatus);

        return ResponseEntity.status(status).body(processingResult);
    }

    /**
     * Abhängig von der Validität des VoidProcessingREsults wird der entsprechende HttpStatus gesetzt.
     */
    public static ResponseEntity<VoidProcessingResult> build(VoidProcessingResult processingResult,
            HttpStatus validStatus, HttpStatus invalidStatus)
    {
        return ResponseEntity.status(processingResult.isValid() ? validStatus : invalidStatus)
                .body(processingResult);
    }

    /**
     * Der invalidStatus wird zusätzlich in NotFound und Other unterteilt.
     */
    public static ResponseEntity<VoidProcessingResult> build(VoidProcessingResult processingResult,
            HttpStatus validStatus, HttpStatus invalidStatus, HttpStatus notFoundStatus)
    {
        HttpStatus status = buildStatus(processingResult, validStatus, invalidStatus,
                notFoundStatus);

        return ResponseEntity.status(status).body(processingResult);
    }

    private static HttpStatus buildStatus(ProcessingResult<?> processingResult, HttpStatus validStatus,
            HttpStatus invalidStatus, HttpStatus notFoundStatus)
    {
        HttpStatus status;

        if (processingResult.isValid())
        {
            status = validStatus;
        }
        else
        {
            if (processingResult.getErrors().size() == 1 && (ErrorKey.isNotFoundError(
                    processingResult.getErrors().get(0))))
            {
                status = notFoundStatus;
            }
            else
            {
                status = invalidStatus;
            }
        }

        return status;
    }
}
