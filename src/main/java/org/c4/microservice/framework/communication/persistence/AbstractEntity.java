package org.c4.microservice.framework.communication.persistence;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.c4.microservice.framework.domainevent.communication.CreateDomainEvent;
import org.springframework.data.annotation.Id;

import java.time.Instant;

/**
 * Wrapper für alle Objekte, die in der Datenbank persistiert werden sollen.
 * <br/>
 * Copyright: Copyright (c) 18.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEntity
{
    @Id
    private String  aggregateId;
    private Long    version;
    private String  createdBy;
    private Instant createdAt;
    private String  lastModifiedBy;
    private Instant lastModifiedAt;

    public AbstractEntity(CreateDomainEvent createDomainEvent)
    {
        aggregateId = createDomainEvent.getAggregateId();
        createdAt = createDomainEvent.getCreatedAt();
        createdBy = createDomainEvent.getCreatedBy();
        lastModifiedAt = createDomainEvent.getCreatedAt();
        lastModifiedBy = createDomainEvent.getCreatedBy();
        version = createDomainEvent.getVersion();
    }
}
