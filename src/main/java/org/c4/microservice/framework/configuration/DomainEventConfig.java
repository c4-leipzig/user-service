package org.c4.microservice.framework.configuration;

import org.c4.microservice.framework.domainevent.repository.DomainEventRepository;
import org.c4.microservice.framework.domainevent.repository.DomainEventRepositoryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * <br/>
 * Copyright: Copyright (c) 14.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
public class DomainEventConfig
{
    @Bean
    public DomainEventRepository domainEventRepository(WebClient webClient,
            EventstoreProperties eventstoreProperties)
    {
        return new DomainEventRepositoryImpl(webClient, eventstoreProperties);
    }
}
