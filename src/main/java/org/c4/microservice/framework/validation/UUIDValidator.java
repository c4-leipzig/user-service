package org.c4.microservice.framework.validation;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Utility-Class zur Bereitstellung eines UUID-Validierungspatterns.
 * <br/>
 * Copyright: Copyright (c) 14.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UUIDValidator
{
    public static final String UUID_PATTERN = "^[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}$";
}
