package org.c4.microservice.framework.validation.enums;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.*;

/**
 * Validierungs-Annotation mit der sichergestellt wird, dass ein String einen gültigen Enum-Wert enthält.
 * <br/>
 * Copyright: Copyright (c) 18.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Documented
@Constraint(validatedBy = EnumValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@ReportAsSingleViolation
public @interface EnumValue
{

    Class<? extends Enum<?>> enumClazz();

    String message() default "Value is not valid";

    boolean nullable() default false;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}

