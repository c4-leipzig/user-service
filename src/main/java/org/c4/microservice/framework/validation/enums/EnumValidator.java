package org.c4.microservice.framework.validation.enums;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Validator-Implementation für die {@link EnumValue}-Annotation.
 * <br/>
 * Copyright: Copyright (c) 18.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class EnumValidator implements ConstraintValidator<EnumValue, String>
{
    private List<String> valueList = new ArrayList<>();
    private boolean      nullable;

    @Override
    public void initialize(EnumValue enumValue)
    {
        Class<? extends Enum<?>> enumClass = enumValue.enumClazz();

        valueList = Arrays.stream(enumClass.getEnumConstants())
                .map(Enum::toString)
                .collect(Collectors.toList());

        nullable = enumValue.nullable();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context)
    {
        return value == null ? nullable : valueList.contains(value);
    }
}
