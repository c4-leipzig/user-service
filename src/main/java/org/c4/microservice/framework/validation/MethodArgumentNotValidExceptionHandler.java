package org.c4.microservice.framework.validation;

import org.c4.microservice.framework.communication.VoidProcessingResult;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * ErrorHandling für Validation-Errors.
 * Gescheiterte Validierungen werden als Liste von ErrorKeys ausgegeben.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@ControllerAdvice
public class MethodArgumentNotValidExceptionHandler extends ResponseEntityExceptionHandler
{
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status,
            WebRequest request)
    {
        List<String> errors = new ArrayList<>();
        for (FieldError error : ex.getBindingResult().getFieldErrors())
        {
            errors.add(error.getDefaultMessage());
        }

        return ResponseEntity.status(status)
                .headers(headers)
                .body(VoidProcessingResult.ofErrors(errors));
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request)
    {
        List<String> errors = new ArrayList<>();
        for (FieldError error : ex.getFieldErrors())
        {
            errors.add(error.getDefaultMessage());
        }
        return ResponseEntity.status(status)
                .headers(headers)
                .body(VoidProcessingResult.ofErrors(errors));
    }
}
