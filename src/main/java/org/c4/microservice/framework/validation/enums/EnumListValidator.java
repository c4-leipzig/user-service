package org.c4.microservice.framework.validation.enums;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Validator für die {@link EnumValues}-Annotation.
 * <br/>
 * Copyright: Copyright (c) 20.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class EnumListValidator implements ConstraintValidator<EnumValues, List<String>>
{
    private List<String> valueList;
    private boolean      nullable;

    @Override
    public void initialize(EnumValues enumValues)
    {
        Class<? extends Enum<?>> enumClass = enumValues.enumClazz();

        valueList = Arrays.stream(enumClass.getEnumConstants())
                .map(Enum::toString)
                .collect(Collectors.toList());

        nullable = enumValues.nullable();
    }

    @Override
    public boolean isValid(List<String> value, ConstraintValidatorContext context)
    {
        return (value == null || value.isEmpty()) ? nullable : valueList.containsAll(value);
    }
}
