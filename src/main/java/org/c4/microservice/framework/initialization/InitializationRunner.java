package org.c4.microservice.framework.initialization;

import lombok.extern.log4j.Log4j2;
import org.c4.microservice.framework.domainevent.DomainEventProcessingState;
import org.c4.microservice.framework.domainevent.communication.CreateDomainEvent;
import org.c4.microservice.framework.domainevent.communication.DomainEvent;
import org.c4.microservice.framework.domainevent.repository.DomainEventRepository;
import org.c4.userservice.domainevent.DomainEventHandlingContext;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Bean zur initialen Aufdatung des Services. Die Operationen sind bewusst Blocking und der KafkaListener wartet,
 * bis dieser Bean fertig ist. Damit wird sichergestellt, dass der Service erst nach seiner Aufdatung
 * mit der Verarbeitung anfängt.
 * <br/>
 * Copyright: Copyright (c) 06.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
public class InitializationRunner
{
    private final DomainEventRepository domainEventRepository;

    private final DomainEventHandlingContext handlingContext;

    public InitializationRunner(DomainEventRepository domainEventRepository,
            DomainEventHandlingContext handlingContext)
    {
        this.domainEventRepository = domainEventRepository;
        this.handlingContext = handlingContext;
    }

    public void run()
    {
        domainEventRepository.getRelevantEvents(getCreateEvents()).forEach(this::handleEvents);
    }

    private List<String> getCreateEvents()
    {
        List<String> result = new ArrayList<>();

        ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(
                false);
        provider.addIncludeFilter(new AssignableTypeFilter(CreateDomainEvent.class));

        Set<BeanDefinition> components = provider.findCandidateComponents("org/c4/userservice");
        components.forEach(beanDefinition -> {
            String fullClassName = beanDefinition.getBeanClassName();
            if (fullClassName != null)
            {
                String[] nameParts = beanDefinition.getBeanClassName().split("\\.");
                result.add(nameParts[nameParts.length - 1]);
            }
        });

        return result;
    }

    private void handleEvents(List<DomainEvent> domainEvents)
    {
        if (domainEvents == null || domainEvents.isEmpty())
        {
            return;
        }

        log.debug("Processing aggregate '{}'", domainEvents.get(0).getAggregateId());
        for (DomainEvent domainEvent : domainEvents)
        {
            DomainEventProcessingState processingState = domainEvent.handleEvent(handlingContext);
            if (processingState == DomainEventProcessingState.VERSION_MISMATCH)
            {
                return;
            }
        }
    }
}
