package org.c4.microservice.framework.domainevent.communication;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.c4.microservice.framework.domainevent.DomainEventProcessingState;
import org.c4.userservice.domainevent.DomainEventHandlingContext;

/**
 * Sammelstelle für alle Events, die nichts mit diesem Service zu tun haben.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@Getter
@Setter
@NoArgsConstructor
public class UnknownEvent extends DomainEvent
{
    private String classIdentifier;

    @Override
    public DomainEventProcessingState handleEvent(DomainEventHandlingContext handlingContext)
    {
        log.debug("Discarding unknown event of type '{}'", classIdentifier);
        return null;
    }
}
