package org.c4.microservice.framework.domainevent.communication;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.c4.microservice.framework.domainevent.DomainEventProcessingState;
import org.c4.userservice.domainevent.DomainEventHandlingContext;
import org.c4.userservice.user.events.UserCreatedEvent;
import org.c4.userservice.user.events.UserStatusUpdatedEvent;
import org.c4.userservice.user.mapping.events.UserMappingUpdatedEvent;

import java.time.Instant;

/**
 * Abstrakte Oberklasse für alle DomainEvents.
 * <br/>
 * Copyright: Copyright (c) 25.02.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, defaultImpl = UnknownEvent.class, property = "classIdentifier")
@JsonSubTypes({ @JsonSubTypes.Type(value = UserCreatedEvent.class, name = "UserCreatedEvent"),
        @JsonSubTypes.Type(value = UserMappingUpdatedEvent.class, name = "UserMappingUpdatedEvent"),
        @JsonSubTypes.Type(value = UserStatusUpdatedEvent.class, name = "UserStatusUpdatedEvent") })
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Log4j2
public abstract class DomainEvent
{
    // Wird immer vom erstellenden Service gesetzt
    protected String aggregateId;
    protected String createdBy;

    // Wird vom Eventstore beim tatsächlichen Einfügen gesetzt
    protected Instant createdAt;

    // Wird vom Eventstore vorgegeben
    protected Long version;

    /**
     * Methode zum Verarbeiten des Events. Diese prüft auf Gültigkeit des Events und ruft anschließend
     * die entsprechende apply-Methode auf.
     */
    public abstract DomainEventProcessingState handleEvent(
            DomainEventHandlingContext handlingContext);
}
