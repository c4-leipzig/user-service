package org.c4.microservice.framework.domainevent.messaging;

import org.c4.microservice.framework.domainevent.communication.DomainEvent;
import org.springframework.kafka.core.KafkaTemplate;

/**
 * Publisher zum Veröffentlichen von Events mittels Kafka.
 * <br/>
 * Copyright: Copyright (c) 19.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class EventPublisherImpl implements EventPublisher
{
    private final KafkaTemplate<String, DomainEvent> kafkaTemplate;
    private final String                             domainEventProducerTopic;

    public EventPublisherImpl(KafkaTemplate<String, DomainEvent> kafkaTemplate,
            String domainEventProducerTopic)
    {
        this.kafkaTemplate = kafkaTemplate;
        this.domainEventProducerTopic = domainEventProducerTopic;
    }

    @Override
    public void send(DomainEvent domainEvent)
    {
        kafkaTemplate.send(domainEventProducerTopic, domainEvent);
    }
}
