package org.c4.microservice.framework.domainevent;

import org.c4.microservice.framework.domainevent.communication.DomainEvent;
import org.c4.userservice.domainevent.DomainEventHandlingContext;
import org.springframework.kafka.annotation.KafkaListener;

/**
 * KafkaListener für alle {@link DomainEvent}s.
 * <br/>
 * Copyright: Copyright (c) 17.12.2019 <br/>
 * Organisation: Exxeta GmbH
 *
 * @author Jan Buchholz <a href="mailto:jan.buchholz@exxeta.com">jan.buchholz@exxeta.com</a>
 */
public class KafkaAsyncDomainEventSinkImpl implements DomainEventSink
{
    private final DomainEventHandlingContext domainEventHandlingContext;

    public KafkaAsyncDomainEventSinkImpl(DomainEventHandlingContext domainEventHandlingContext)
    {
        this.domainEventHandlingContext = domainEventHandlingContext;
    }

    @KafkaListener(topics = "domainEventFromEventStoreTopic")
    public void consumeDomainEvent(DomainEvent domainEvent)
    {
        domainEvent.handleEvent(domainEventHandlingContext);
    }
}
