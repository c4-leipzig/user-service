package org.c4.microservice.framework.domainevent.communication;

import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.bson.types.ObjectId;
import org.c4.microservice.framework.domainevent.DomainEventProcessingState;
import org.c4.userservice.domainevent.DomainEventHandlingContext;

import java.security.Principal;

import static org.c4.microservice.framework.domainevent.DomainEventProcessingState.OBSOLETE;
import static org.c4.microservice.framework.domainevent.DomainEventProcessingState.PROCESSED;

/**
 * Oberklasse für CREATE-Events.
 * <br/>
 * Copyright: Copyright (c) 14.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@NoArgsConstructor
public abstract class CreateDomainEvent extends DomainEvent
{
    public CreateDomainEvent(Principal createdBy)
    {
        this(new ObjectId().toHexString(), createdBy);
    }

    public CreateDomainEvent(String aggregateId, Principal createdBy)
    {
        super();
        this.aggregateId = aggregateId;
        this.createdBy = createdBy.getName();
    }

    @Override
    public DomainEventProcessingState handleEvent(DomainEventHandlingContext handlingContext)
    {
        if (handlingContext.isInitializing() && aggregatePresent(handlingContext))
        {
            log.info("CreateEvent {} is obsolete.", getClass().getSimpleName());
            return OBSOLETE;
        }

        apply(handlingContext);
        return PROCESSED;
    }

    protected abstract void apply(DomainEventHandlingContext handlingContext);

    protected abstract boolean aggregatePresent(DomainEventHandlingContext handlingContext);
}
