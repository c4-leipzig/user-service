package org.c4.microservice.framework.domainevent.communication;

import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.c4.microservice.framework.communication.persistence.AbstractEntity;
import org.c4.microservice.framework.domainevent.DomainEventProcessingState;
import org.c4.userservice.domainevent.DomainEventHandlingContext;

import java.util.Optional;

import static org.c4.microservice.framework.domainevent.DomainEventProcessingState.*;

/**
 * Oberklasse für UPDATE-Events.
 * <br/>
 * Copyright: Copyright (c) 18.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Log4j2
@NoArgsConstructor
public abstract class UpdateDomainEvent extends DomainEvent
{
    public UpdateDomainEvent(String aggregateId, String createdBy)
    {
        super();
        this.aggregateId = aggregateId;
        this.createdBy = createdBy;
    }

    public UpdateDomainEvent(String aggregateId, String createdBy, long version)
    {
        super();
        this.aggregateId = aggregateId;
        this.createdBy = createdBy;
        this.version = version;
    }

    @Override
    public DomainEventProcessingState handleEvent(DomainEventHandlingContext handlingContext)
    {
        DomainEventProcessingState processingState;

        Optional<? extends AbstractEntity> maybeAggregate = getAggregate(handlingContext);
        if (maybeAggregate.isEmpty())
        {
            processingState = VERSION_MISMATCH;
            handleVersionMismatch(handlingContext, null);
        }
        else
        {
            AbstractEntity aggregate = maybeAggregate.get();
            Long currentVersion = aggregate.getVersion();

            if (version <= currentVersion)
            {
                processingState = OBSOLETE;
                log.info("Update-Event {} is obsolete. Event version: {}, Current version: {}",
                        getClass().getSimpleName(), version, currentVersion);
            }
            else if (version == currentVersion + 1)
            {
                processingState = PROCESSED;
                apply(handlingContext, aggregate);
            }
            else
            {
                processingState = VERSION_MISMATCH;
                handleVersionMismatch(handlingContext, currentVersion);
            }
        }

        return processingState;
    }

    protected abstract void apply(DomainEventHandlingContext handlingContext,
            AbstractEntity aggregate);

    protected abstract Optional<? extends AbstractEntity> getAggregate(
            DomainEventHandlingContext handlingContext);

    private void handleVersionMismatch(DomainEventHandlingContext handlingContext,
            Long currentVersion)
    {
        log.warn("Version mismatch for aggregate '{}'. Event version: {}, Current version: {}",
                aggregateId, version, currentVersion);
        handlingContext.reloadAggregate(aggregateId,
                currentVersion == null ? 0L : currentVersion + 1);
    }

}
