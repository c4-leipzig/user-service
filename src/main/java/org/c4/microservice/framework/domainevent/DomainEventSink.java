package org.c4.microservice.framework.domainevent;

import org.c4.microservice.framework.domainevent.communication.DomainEvent;

/**
 * Interface für die Entgegennahme von Events.
 * <br/>
 * Copyright: Copyright (c) 15.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface DomainEventSink
{
    void consumeDomainEvent(DomainEvent domainEvent);
}
